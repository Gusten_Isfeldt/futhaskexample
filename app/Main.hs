{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
module Main where

import Algebra
import Algebra.Entries
import Algebra.Types
import qualified Data.Massiv.Array as M

mOp mf ma mb = do
    a <- ma
    b <- mb
    mf a b

type FVector c = FT c (F32_1d c)
toFVector :: M.Array M.S M.Ix1 Float -> FVector c
toFVector = toFuthark

fromFVector :: FVector c -> FT c (M.Array M.S M.Ix1 Float)
fromFVector = (>>= fromFuthark)

instance Num (FVector c) where
    (-) = mOp vectorSub
    (+) = mOp vectorAdd
    (*) = mOp vectorMul
    abs = (>>= vectorAbs)
    signum = (>>= vectorSgn)
    fromInteger = toFVector.M.singleton.fromInteger


type FMatrix c = FT c (F32_2d c)
toFMatrix :: M.Array M.S M.Ix2 Float -> FMatrix c
toFMatrix = toFuthark

fromFMatrix :: FMatrix c -> FT c (M.Array M.S M.Ix2 Float)
fromFMatrix = (>>= fromFuthark)

instance Num (FMatrix c) where
    (-) = mOp matrixSub
    (+) = mOp matrixAdd
    (*) = mOp matrixMul
    abs = (>>= matrixAbs)
    signum = (>>= matrixSgn)
    fromInteger = toFMatrix.M.singleton.fromInteger

(<.>) = mOp dot
(!*) = mOp matrixVectorMul 
(*!) = mOp vectorMatrixMul
(!*!) = mOp matrixMatrixMul


main :: IO ()
main = do
    putStrLn "test"
    c <- getContext []
    print $ runFTIn c $ a <.> b
    print $ runFTIn c $ fromFVector $ a * b
    print $ runFTIn c $ fromFMatrix $ m1 * m2
    print $ runFTIn c $ fromFMatrix $ m1 !*! m2
    print $ runFTIn c $ fromFVector $ a *! m2
    print $ runFTIn c $ fromFVector $ m1 !* b
    where a = toFVector $ M.fromLists' M.Par [1, 2, 3]
          b = toFVector $ M.fromLists' M.Par [4, 5, 6]
          m1 = toFMatrix $ M.fromLists' M.Par 
               [ [ 1, 2, 3]
               , [ 4, 5, 6]
               , [ 7, 8, 9] ]
          m2 = toFMatrix $ M.fromLists' M.Par 
               [ [ 1, 1, 1]
               , [ 1, 1, 1]
               , [ 1, 1, 1] ]
