#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>
#undef NDEBUG
#include <assert.h>
// Start of panic.h.

#include <stdarg.h>

static const char *fut_progname;

static void panic(int eval, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
        fprintf(stderr, "%s: ", fut_progname);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
        exit(eval);
}

/* For generating arbitrary-sized error messages.  It is the callers
   responsibility to free the buffer at some point. */
static char* msgprintf(const char *s, ...) {
  va_list vl;
  va_start(vl, s);
  size_t needed = 1 + vsnprintf(NULL, 0, s, vl);
  char *buffer = (char*) malloc(needed);
  va_start(vl, s); /* Must re-init. */
  vsnprintf(buffer, needed, s, vl);
  return buffer;
}

// End of panic.h.

// Start of timing.h.

// The function get_wall_time() returns the wall time in microseconds
// (with an unspecified offset).

#ifdef _WIN32

#include <windows.h>

static int64_t get_wall_time(void) {
  LARGE_INTEGER time,freq;
  assert(QueryPerformanceFrequency(&freq));
  assert(QueryPerformanceCounter(&time));
  return ((double)time.QuadPart / freq.QuadPart) * 1000000;
}

#else
/* Assuming POSIX */

#include <time.h>
#include <sys/time.h>

static int64_t get_wall_time(void) {
  struct timeval time;
  assert(gettimeofday(&time,NULL) == 0);
  return time.tv_sec * 1000000 + time.tv_usec;
}

#endif

// End of timing.h.

#ifdef _MSC_VER
#define inline __inline
#endif
#include <string.h>
#include <inttypes.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
// Start of lock.h.

/* A very simple cross-platform implementation of locks.  Uses
   pthreads on Unix and some Windows thing there.  Futhark's
   host-level code is not multithreaded, but user code may be, so we
   need some mechanism for ensuring atomic access to API functions.
   This is that mechanism.  It is not exposed to user code at all, so
   we do not have to worry about name collisions. */

#ifdef _WIN32

typedef HANDLE lock_t;

static lock_t create_lock(lock_t *lock) {
  *lock = CreateMutex(NULL,  /* Default security attributes. */
                      FALSE, /* Initially unlocked. */
                      NULL); /* Unnamed. */
}

static void lock_lock(lock_t *lock) {
  assert(WaitForSingleObject(*lock, INFINITE) == WAIT_OBJECT_0);
}

static void lock_unlock(lock_t *lock) {
  assert(ReleaseMutex(*lock));
}

static void free_lock(lock_t *lock) {
  CloseHandle(*lock);
}

#else
/* Assuming POSIX */

#include <pthread.h>

typedef pthread_mutex_t lock_t;

static void create_lock(lock_t *lock) {
  int r = pthread_mutex_init(lock, NULL);
  assert(r == 0);
}

static void lock_lock(lock_t *lock) {
  int r = pthread_mutex_lock(lock);
  assert(r == 0);
}

static void lock_unlock(lock_t *lock) {
  int r = pthread_mutex_unlock(lock);
  assert(r == 0);
}

static void free_lock(lock_t *lock) {
  /* Nothing to do for pthreads. */
  (void)lock;
}

#endif

// End of lock.h.

struct memblock {
    int *references;
    char *mem;
    int64_t size;
    const char *desc;
} ;
struct futhark_context_config {
    int debugging;
} ;
struct futhark_context_config *futhark_context_config_new()
{
    struct futhark_context_config *cfg =
                                  (struct futhark_context_config *) malloc(sizeof(struct futhark_context_config));
    
    if (cfg == NULL)
        return NULL;
    cfg->debugging = 0;
    return cfg;
}
void futhark_context_config_free(struct futhark_context_config *cfg)
{
    free(cfg);
}
void futhark_context_config_set_debugging(struct futhark_context_config *cfg,
                                          int detail)
{
    cfg->debugging = detail;
}
void futhark_context_config_set_logging(struct futhark_context_config *cfg,
                                        int detail)
{
    /* Does nothing for this backend. */
    cfg = cfg;
    detail = detail;
}
struct futhark_context {
    int detail_memory;
    int debugging;
    int profiling;
    lock_t lock;
    char *error;
    int64_t peak_mem_usage_default;
    int64_t cur_mem_usage_default;
} ;
struct futhark_context *futhark_context_new(struct futhark_context_config *cfg)
{
    struct futhark_context *ctx =
                           (struct futhark_context *) malloc(sizeof(struct futhark_context));
    
    if (ctx == NULL)
        return NULL;
    ctx->detail_memory = cfg->debugging;
    ctx->debugging = cfg->debugging;
    ctx->error = NULL;
    create_lock(&ctx->lock);
    ctx->peak_mem_usage_default = 0;
    ctx->cur_mem_usage_default = 0;
    return ctx;
}
void futhark_context_free(struct futhark_context *ctx)
{
    free_lock(&ctx->lock);
    free(ctx);
}
int futhark_context_sync(struct futhark_context *ctx)
{
    ctx = ctx;
    return 0;
}
char *futhark_context_get_error(struct futhark_context *ctx)
{
    char *error = ctx->error;
    
    ctx->error = NULL;
    return error;
}
void futhark_context_pause_profiling(struct futhark_context *ctx)
{
    (void) ctx;
}
void futhark_context_unpause_profiling(struct futhark_context *ctx)
{
    (void) ctx;
}
static int memblock_unref(struct futhark_context *ctx, struct memblock *block,
                          const char *desc)
{
    if (block->references != NULL) {
        *block->references -= 1;
        if (ctx->detail_memory)
            fprintf(stderr,
                    "Unreferencing block %s (allocated as %s) in %s: %d references remaining.\n",
                    desc, block->desc, "default space", *block->references);
        if (*block->references == 0) {
            ctx->cur_mem_usage_default -= block->size;
            free(block->mem);
            free(block->references);
            if (ctx->detail_memory)
                fprintf(stderr,
                        "%lld bytes freed (now allocated: %lld bytes)\n",
                        (long long) block->size,
                        (long long) ctx->cur_mem_usage_default);
        }
        block->references = NULL;
    }
    return 0;
}
static int memblock_alloc(struct futhark_context *ctx, struct memblock *block,
                          int64_t size, const char *desc)
{
    if (size < 0)
        panic(1, "Negative allocation of %lld bytes attempted for %s in %s.\n",
              (long long) size, desc, "default space",
              ctx->cur_mem_usage_default);
    
    int ret = memblock_unref(ctx, block, desc);
    
    ctx->cur_mem_usage_default += size;
    if (ctx->detail_memory)
        fprintf(stderr,
                "Allocating %lld bytes for %s in %s (then allocated: %lld bytes)",
                (long long) size, desc, "default space",
                (long long) ctx->cur_mem_usage_default);
    if (ctx->cur_mem_usage_default > ctx->peak_mem_usage_default) {
        ctx->peak_mem_usage_default = ctx->cur_mem_usage_default;
        if (ctx->detail_memory)
            fprintf(stderr, " (new peak).\n");
    } else if (ctx->detail_memory)
        fprintf(stderr, ".\n");
    block->mem = (char *) malloc(size);
    block->references = (int *) malloc(sizeof(int));
    *block->references = 1;
    block->size = size;
    block->desc = desc;
    return ret;
}
static int memblock_set(struct futhark_context *ctx, struct memblock *lhs,
                        struct memblock *rhs, const char *lhs_desc)
{
    int ret = memblock_unref(ctx, lhs, lhs_desc);
    
    (*rhs->references)++;
    *lhs = *rhs;
    return ret;
}
void futhark_debugging_report(struct futhark_context *ctx)
{
    if (ctx->detail_memory || ctx->profiling) {
        fprintf(stderr, "Peak memory usage for default space: %lld bytes.\n",
                (long long) ctx->peak_mem_usage_default);
    }
    if (ctx->profiling) { }
}
static int futrts_matrixMatrixMul(struct futhark_context *ctx,
                                  struct memblock *out_mem_p_11759,
                                  int32_t *out_out_arrsizze_11760,
                                  int32_t *out_out_arrsizze_11761,
                                  struct memblock A_mem_11670,
                                  struct memblock B_mem_11671, int32_t i_11623,
                                  int32_t j_11624, int32_t j_11625,
                                  int32_t k_11626);
static int futrts_vectorMatrixMul(struct futhark_context *ctx,
                                  struct memblock *out_mem_p_11762,
                                  int32_t *out_out_arrsizze_11763,
                                  struct memblock v_mem_11670,
                                  struct memblock M_mem_11671, int32_t i_11598,
                                  int32_t i_11599, int32_t j_11600);
static int futrts_matrixMul(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11764,
                            int32_t *out_out_arrsizze_11765,
                            int32_t *out_out_arrsizze_11766,
                            struct memblock A_mem_11670,
                            struct memblock B_mem_11671, int32_t i_11564,
                            int32_t j_11565, int32_t i_11566, int32_t j_11567);
static int futrts_matrixSub(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11767,
                            int32_t *out_out_arrsizze_11768,
                            int32_t *out_out_arrsizze_11769,
                            struct memblock A_mem_11670,
                            struct memblock B_mem_11671, int32_t i_11530,
                            int32_t j_11531, int32_t i_11532, int32_t j_11533);
static int futrts_matrixAdd(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11770,
                            int32_t *out_out_arrsizze_11771,
                            int32_t *out_out_arrsizze_11772,
                            struct memblock A_mem_11670,
                            struct memblock B_mem_11671, int32_t i_11496,
                            int32_t j_11497, int32_t i_11498, int32_t j_11499);
static int futrts_matrixVectorMul(struct futhark_context *ctx,
                                  struct memblock *out_mem_p_11773,
                                  int32_t *out_out_arrsizze_11774,
                                  struct memblock M_mem_11670,
                                  struct memblock v_mem_11671, int32_t i_11475,
                                  int32_t j_11476, int32_t sizze_11477);
static int futrts_normalizzeV(struct futhark_context *ctx,
                              struct memblock *out_mem_p_11775,
                              int32_t *out_out_arrsizze_11776,
                              struct memblock v_mem_11670, int32_t sizze_11462);
static int futrts_vectorNorm(struct futhark_context *ctx,
                             float *out_scalar_out_11777,
                             struct memblock v_mem_11670, int32_t sizze_11453);
static int futrts_dot(struct futhark_context *ctx, float *out_scalar_out_11778,
                      struct memblock a_mem_11670, struct memblock b_mem_11671,
                      int32_t i_11435, int32_t i_11436);
static int futrts_matrixSgn(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11779,
                            int32_t *out_out_arrsizze_11780,
                            int32_t *out_out_arrsizze_11781,
                            struct memblock x_mem_11670, int32_t sizze_11424,
                            int32_t sizze_11425);
static int futrts_scaleM(struct futhark_context *ctx,
                         struct memblock *out_mem_p_11782,
                         int32_t *out_out_arrsizze_11783,
                         int32_t *out_out_arrsizze_11784,
                         struct memblock x_mem_11670, int32_t sizze_11415,
                         int32_t sizze_11416, float f_11417);
static int futrts_matrixAbs(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11785,
                            int32_t *out_out_arrsizze_11786,
                            int32_t *out_out_arrsizze_11787,
                            struct memblock x_mem_11670, int32_t sizze_11407,
                            int32_t sizze_11408);
static int futrts_vectorMul(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11788,
                            int32_t *out_out_arrsizze_11789,
                            struct memblock a_mem_11670,
                            struct memblock b_mem_11671, int32_t i_11392,
                            int32_t i_11393);
static int futrts_vectorSub(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11790,
                            int32_t *out_out_arrsizze_11791,
                            struct memblock a_mem_11670,
                            struct memblock b_mem_11671, int32_t i_11377,
                            int32_t i_11378);
static int futrts_vectorAdd(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11792,
                            int32_t *out_out_arrsizze_11793,
                            struct memblock a_mem_11670,
                            struct memblock b_mem_11671, int32_t i_11362,
                            int32_t i_11363);
static int futrts_vectorSgn(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11794,
                            int32_t *out_out_arrsizze_11795,
                            struct memblock x_mem_11670, int32_t sizze_11354);
static int futrts_scaleV(struct futhark_context *ctx,
                         struct memblock *out_mem_p_11796,
                         int32_t *out_out_arrsizze_11797,
                         struct memblock x_mem_11670, int32_t sizze_11348,
                         float f_11349);
static int futrts_vectorAbs(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11798,
                            int32_t *out_out_arrsizze_11799,
                            struct memblock x_mem_11670, int32_t sizze_11343);
static inline int8_t add8(int8_t x, int8_t y)
{
    return x + y;
}
static inline int16_t add16(int16_t x, int16_t y)
{
    return x + y;
}
static inline int32_t add32(int32_t x, int32_t y)
{
    return x + y;
}
static inline int64_t add64(int64_t x, int64_t y)
{
    return x + y;
}
static inline int8_t sub8(int8_t x, int8_t y)
{
    return x - y;
}
static inline int16_t sub16(int16_t x, int16_t y)
{
    return x - y;
}
static inline int32_t sub32(int32_t x, int32_t y)
{
    return x - y;
}
static inline int64_t sub64(int64_t x, int64_t y)
{
    return x - y;
}
static inline int8_t mul8(int8_t x, int8_t y)
{
    return x * y;
}
static inline int16_t mul16(int16_t x, int16_t y)
{
    return x * y;
}
static inline int32_t mul32(int32_t x, int32_t y)
{
    return x * y;
}
static inline int64_t mul64(int64_t x, int64_t y)
{
    return x * y;
}
static inline uint8_t udiv8(uint8_t x, uint8_t y)
{
    return x / y;
}
static inline uint16_t udiv16(uint16_t x, uint16_t y)
{
    return x / y;
}
static inline uint32_t udiv32(uint32_t x, uint32_t y)
{
    return x / y;
}
static inline uint64_t udiv64(uint64_t x, uint64_t y)
{
    return x / y;
}
static inline uint8_t umod8(uint8_t x, uint8_t y)
{
    return x % y;
}
static inline uint16_t umod16(uint16_t x, uint16_t y)
{
    return x % y;
}
static inline uint32_t umod32(uint32_t x, uint32_t y)
{
    return x % y;
}
static inline uint64_t umod64(uint64_t x, uint64_t y)
{
    return x % y;
}
static inline int8_t sdiv8(int8_t x, int8_t y)
{
    int8_t q = x / y;
    int8_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int16_t sdiv16(int16_t x, int16_t y)
{
    int16_t q = x / y;
    int16_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int32_t sdiv32(int32_t x, int32_t y)
{
    int32_t q = x / y;
    int32_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int64_t sdiv64(int64_t x, int64_t y)
{
    int64_t q = x / y;
    int64_t r = x % y;
    
    return q - ((r != 0 && r < 0 != y < 0) ? 1 : 0);
}
static inline int8_t smod8(int8_t x, int8_t y)
{
    int8_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int16_t smod16(int16_t x, int16_t y)
{
    int16_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int32_t smod32(int32_t x, int32_t y)
{
    int32_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int64_t smod64(int64_t x, int64_t y)
{
    int64_t r = x % y;
    
    return r + (r == 0 || (x > 0 && y > 0) || (x < 0 && y < 0) ? 0 : y);
}
static inline int8_t squot8(int8_t x, int8_t y)
{
    return x / y;
}
static inline int16_t squot16(int16_t x, int16_t y)
{
    return x / y;
}
static inline int32_t squot32(int32_t x, int32_t y)
{
    return x / y;
}
static inline int64_t squot64(int64_t x, int64_t y)
{
    return x / y;
}
static inline int8_t srem8(int8_t x, int8_t y)
{
    return x % y;
}
static inline int16_t srem16(int16_t x, int16_t y)
{
    return x % y;
}
static inline int32_t srem32(int32_t x, int32_t y)
{
    return x % y;
}
static inline int64_t srem64(int64_t x, int64_t y)
{
    return x % y;
}
static inline int8_t smin8(int8_t x, int8_t y)
{
    return x < y ? x : y;
}
static inline int16_t smin16(int16_t x, int16_t y)
{
    return x < y ? x : y;
}
static inline int32_t smin32(int32_t x, int32_t y)
{
    return x < y ? x : y;
}
static inline int64_t smin64(int64_t x, int64_t y)
{
    return x < y ? x : y;
}
static inline uint8_t umin8(uint8_t x, uint8_t y)
{
    return x < y ? x : y;
}
static inline uint16_t umin16(uint16_t x, uint16_t y)
{
    return x < y ? x : y;
}
static inline uint32_t umin32(uint32_t x, uint32_t y)
{
    return x < y ? x : y;
}
static inline uint64_t umin64(uint64_t x, uint64_t y)
{
    return x < y ? x : y;
}
static inline int8_t smax8(int8_t x, int8_t y)
{
    return x < y ? y : x;
}
static inline int16_t smax16(int16_t x, int16_t y)
{
    return x < y ? y : x;
}
static inline int32_t smax32(int32_t x, int32_t y)
{
    return x < y ? y : x;
}
static inline int64_t smax64(int64_t x, int64_t y)
{
    return x < y ? y : x;
}
static inline uint8_t umax8(uint8_t x, uint8_t y)
{
    return x < y ? y : x;
}
static inline uint16_t umax16(uint16_t x, uint16_t y)
{
    return x < y ? y : x;
}
static inline uint32_t umax32(uint32_t x, uint32_t y)
{
    return x < y ? y : x;
}
static inline uint64_t umax64(uint64_t x, uint64_t y)
{
    return x < y ? y : x;
}
static inline uint8_t shl8(uint8_t x, uint8_t y)
{
    return x << y;
}
static inline uint16_t shl16(uint16_t x, uint16_t y)
{
    return x << y;
}
static inline uint32_t shl32(uint32_t x, uint32_t y)
{
    return x << y;
}
static inline uint64_t shl64(uint64_t x, uint64_t y)
{
    return x << y;
}
static inline uint8_t lshr8(uint8_t x, uint8_t y)
{
    return x >> y;
}
static inline uint16_t lshr16(uint16_t x, uint16_t y)
{
    return x >> y;
}
static inline uint32_t lshr32(uint32_t x, uint32_t y)
{
    return x >> y;
}
static inline uint64_t lshr64(uint64_t x, uint64_t y)
{
    return x >> y;
}
static inline int8_t ashr8(int8_t x, int8_t y)
{
    return x >> y;
}
static inline int16_t ashr16(int16_t x, int16_t y)
{
    return x >> y;
}
static inline int32_t ashr32(int32_t x, int32_t y)
{
    return x >> y;
}
static inline int64_t ashr64(int64_t x, int64_t y)
{
    return x >> y;
}
static inline uint8_t and8(uint8_t x, uint8_t y)
{
    return x & y;
}
static inline uint16_t and16(uint16_t x, uint16_t y)
{
    return x & y;
}
static inline uint32_t and32(uint32_t x, uint32_t y)
{
    return x & y;
}
static inline uint64_t and64(uint64_t x, uint64_t y)
{
    return x & y;
}
static inline uint8_t or8(uint8_t x, uint8_t y)
{
    return x | y;
}
static inline uint16_t or16(uint16_t x, uint16_t y)
{
    return x | y;
}
static inline uint32_t or32(uint32_t x, uint32_t y)
{
    return x | y;
}
static inline uint64_t or64(uint64_t x, uint64_t y)
{
    return x | y;
}
static inline uint8_t xor8(uint8_t x, uint8_t y)
{
    return x ^ y;
}
static inline uint16_t xor16(uint16_t x, uint16_t y)
{
    return x ^ y;
}
static inline uint32_t xor32(uint32_t x, uint32_t y)
{
    return x ^ y;
}
static inline uint64_t xor64(uint64_t x, uint64_t y)
{
    return x ^ y;
}
static inline char ult8(uint8_t x, uint8_t y)
{
    return x < y;
}
static inline char ult16(uint16_t x, uint16_t y)
{
    return x < y;
}
static inline char ult32(uint32_t x, uint32_t y)
{
    return x < y;
}
static inline char ult64(uint64_t x, uint64_t y)
{
    return x < y;
}
static inline char ule8(uint8_t x, uint8_t y)
{
    return x <= y;
}
static inline char ule16(uint16_t x, uint16_t y)
{
    return x <= y;
}
static inline char ule32(uint32_t x, uint32_t y)
{
    return x <= y;
}
static inline char ule64(uint64_t x, uint64_t y)
{
    return x <= y;
}
static inline char slt8(int8_t x, int8_t y)
{
    return x < y;
}
static inline char slt16(int16_t x, int16_t y)
{
    return x < y;
}
static inline char slt32(int32_t x, int32_t y)
{
    return x < y;
}
static inline char slt64(int64_t x, int64_t y)
{
    return x < y;
}
static inline char sle8(int8_t x, int8_t y)
{
    return x <= y;
}
static inline char sle16(int16_t x, int16_t y)
{
    return x <= y;
}
static inline char sle32(int32_t x, int32_t y)
{
    return x <= y;
}
static inline char sle64(int64_t x, int64_t y)
{
    return x <= y;
}
static inline int8_t pow8(int8_t x, int8_t y)
{
    int8_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int16_t pow16(int16_t x, int16_t y)
{
    int16_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int32_t pow32(int32_t x, int32_t y)
{
    int32_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline int64_t pow64(int64_t x, int64_t y)
{
    int64_t res = 1, rem = y;
    
    while (rem != 0) {
        if (rem & 1)
            res *= x;
        rem >>= 1;
        x *= x;
    }
    return res;
}
static inline bool itob_i8_bool(int8_t x)
{
    return x;
}
static inline bool itob_i16_bool(int16_t x)
{
    return x;
}
static inline bool itob_i32_bool(int32_t x)
{
    return x;
}
static inline bool itob_i64_bool(int64_t x)
{
    return x;
}
static inline int8_t btoi_bool_i8(bool x)
{
    return x;
}
static inline int16_t btoi_bool_i16(bool x)
{
    return x;
}
static inline int32_t btoi_bool_i32(bool x)
{
    return x;
}
static inline int64_t btoi_bool_i64(bool x)
{
    return x;
}
#define sext_i8_i8(x) ((int8_t) (int8_t) x)
#define sext_i8_i16(x) ((int16_t) (int8_t) x)
#define sext_i8_i32(x) ((int32_t) (int8_t) x)
#define sext_i8_i64(x) ((int64_t) (int8_t) x)
#define sext_i16_i8(x) ((int8_t) (int16_t) x)
#define sext_i16_i16(x) ((int16_t) (int16_t) x)
#define sext_i16_i32(x) ((int32_t) (int16_t) x)
#define sext_i16_i64(x) ((int64_t) (int16_t) x)
#define sext_i32_i8(x) ((int8_t) (int32_t) x)
#define sext_i32_i16(x) ((int16_t) (int32_t) x)
#define sext_i32_i32(x) ((int32_t) (int32_t) x)
#define sext_i32_i64(x) ((int64_t) (int32_t) x)
#define sext_i64_i8(x) ((int8_t) (int64_t) x)
#define sext_i64_i16(x) ((int16_t) (int64_t) x)
#define sext_i64_i32(x) ((int32_t) (int64_t) x)
#define sext_i64_i64(x) ((int64_t) (int64_t) x)
#define zext_i8_i8(x) ((uint8_t) (uint8_t) x)
#define zext_i8_i16(x) ((uint16_t) (uint8_t) x)
#define zext_i8_i32(x) ((uint32_t) (uint8_t) x)
#define zext_i8_i64(x) ((uint64_t) (uint8_t) x)
#define zext_i16_i8(x) ((uint8_t) (uint16_t) x)
#define zext_i16_i16(x) ((uint16_t) (uint16_t) x)
#define zext_i16_i32(x) ((uint32_t) (uint16_t) x)
#define zext_i16_i64(x) ((uint64_t) (uint16_t) x)
#define zext_i32_i8(x) ((uint8_t) (uint32_t) x)
#define zext_i32_i16(x) ((uint16_t) (uint32_t) x)
#define zext_i32_i32(x) ((uint32_t) (uint32_t) x)
#define zext_i32_i64(x) ((uint64_t) (uint32_t) x)
#define zext_i64_i8(x) ((uint8_t) (uint64_t) x)
#define zext_i64_i16(x) ((uint16_t) (uint64_t) x)
#define zext_i64_i32(x) ((uint32_t) (uint64_t) x)
#define zext_i64_i64(x) ((uint64_t) (uint64_t) x)
#ifdef __OPENCL_VERSION__
int32_t futrts_popc8(int8_t x)
{
    return popcount(x);
}
int32_t futrts_popc16(int16_t x)
{
    return popcount(x);
}
int32_t futrts_popc32(int32_t x)
{
    return popcount(x);
}
int32_t futrts_popc64(int64_t x)
{
    return popcount(x);
}
#elif __CUDA_ARCH__
int32_t futrts_popc8(int8_t x)
{
    return __popc(zext_i8_i32(x));
}
int32_t futrts_popc16(int16_t x)
{
    return __popc(zext_i16_i32(x));
}
int32_t futrts_popc32(int32_t x)
{
    return __popc(x);
}
int32_t futrts_popc64(int64_t x)
{
    return __popcll(x);
}
#else
int32_t futrts_popc8(int8_t x)
{
    int c = 0;
    
    for (; x; ++c)
        x &= x - 1;
    return c;
}
int32_t futrts_popc16(int16_t x)
{
    int c = 0;
    
    for (; x; ++c)
        x &= x - 1;
    return c;
}
int32_t futrts_popc32(int32_t x)
{
    int c = 0;
    
    for (; x; ++c)
        x &= x - 1;
    return c;
}
int32_t futrts_popc64(int64_t x)
{
    int c = 0;
    
    for (; x; ++c)
        x &= x - 1;
    return c;
}
#endif
#ifdef __OPENCL_VERSION__
int32_t futrts_clzz8(int8_t x)
{
    return clz(x);
}
int32_t futrts_clzz16(int16_t x)
{
    return clz(x);
}
int32_t futrts_clzz32(int32_t x)
{
    return clz(x);
}
int32_t futrts_clzz64(int64_t x)
{
    return clz(x);
}
#elif __CUDA_ARCH__
int32_t futrts_clzz8(int8_t x)
{
    return __clz(zext_i8_i32(x)) - 24;
}
int32_t futrts_clzz16(int16_t x)
{
    return __clz(zext_i16_i32(x)) - 16;
}
int32_t futrts_clzz32(int32_t x)
{
    return __clz(x);
}
int32_t futrts_clzz64(int64_t x)
{
    return __clzll(x);
}
#else
int32_t futrts_clzz8(int8_t x)
{
    int n = 0;
    int bits = sizeof(x) * 8;
    
    for (int i = 0; i < bits; i++) {
        if (x < 0)
            break;
        n++;
        x <<= 1;
    }
    return n;
}
int32_t futrts_clzz16(int16_t x)
{
    int n = 0;
    int bits = sizeof(x) * 8;
    
    for (int i = 0; i < bits; i++) {
        if (x < 0)
            break;
        n++;
        x <<= 1;
    }
    return n;
}
int32_t futrts_clzz32(int32_t x)
{
    int n = 0;
    int bits = sizeof(x) * 8;
    
    for (int i = 0; i < bits; i++) {
        if (x < 0)
            break;
        n++;
        x <<= 1;
    }
    return n;
}
int32_t futrts_clzz64(int64_t x)
{
    int n = 0;
    int bits = sizeof(x) * 8;
    
    for (int i = 0; i < bits; i++) {
        if (x < 0)
            break;
        n++;
        x <<= 1;
    }
    return n;
}
#endif
static inline float fdiv32(float x, float y)
{
    return x / y;
}
static inline float fadd32(float x, float y)
{
    return x + y;
}
static inline float fsub32(float x, float y)
{
    return x - y;
}
static inline float fmul32(float x, float y)
{
    return x * y;
}
static inline float fmin32(float x, float y)
{
    return fmin(x, y);
}
static inline float fmax32(float x, float y)
{
    return fmax(x, y);
}
static inline float fpow32(float x, float y)
{
    return pow(x, y);
}
static inline char cmplt32(float x, float y)
{
    return x < y;
}
static inline char cmple32(float x, float y)
{
    return x <= y;
}
static inline float sitofp_i8_f32(int8_t x)
{
    return x;
}
static inline float sitofp_i16_f32(int16_t x)
{
    return x;
}
static inline float sitofp_i32_f32(int32_t x)
{
    return x;
}
static inline float sitofp_i64_f32(int64_t x)
{
    return x;
}
static inline float uitofp_i8_f32(uint8_t x)
{
    return x;
}
static inline float uitofp_i16_f32(uint16_t x)
{
    return x;
}
static inline float uitofp_i32_f32(uint32_t x)
{
    return x;
}
static inline float uitofp_i64_f32(uint64_t x)
{
    return x;
}
static inline int8_t fptosi_f32_i8(float x)
{
    return x;
}
static inline int16_t fptosi_f32_i16(float x)
{
    return x;
}
static inline int32_t fptosi_f32_i32(float x)
{
    return x;
}
static inline int64_t fptosi_f32_i64(float x)
{
    return x;
}
static inline uint8_t fptoui_f32_i8(float x)
{
    return x;
}
static inline uint16_t fptoui_f32_i16(float x)
{
    return x;
}
static inline uint32_t fptoui_f32_i32(float x)
{
    return x;
}
static inline uint64_t fptoui_f32_i64(float x)
{
    return x;
}
static inline double fdiv64(double x, double y)
{
    return x / y;
}
static inline double fadd64(double x, double y)
{
    return x + y;
}
static inline double fsub64(double x, double y)
{
    return x - y;
}
static inline double fmul64(double x, double y)
{
    return x * y;
}
static inline double fmin64(double x, double y)
{
    return fmin(x, y);
}
static inline double fmax64(double x, double y)
{
    return fmax(x, y);
}
static inline double fpow64(double x, double y)
{
    return pow(x, y);
}
static inline char cmplt64(double x, double y)
{
    return x < y;
}
static inline char cmple64(double x, double y)
{
    return x <= y;
}
static inline double sitofp_i8_f64(int8_t x)
{
    return x;
}
static inline double sitofp_i16_f64(int16_t x)
{
    return x;
}
static inline double sitofp_i32_f64(int32_t x)
{
    return x;
}
static inline double sitofp_i64_f64(int64_t x)
{
    return x;
}
static inline double uitofp_i8_f64(uint8_t x)
{
    return x;
}
static inline double uitofp_i16_f64(uint16_t x)
{
    return x;
}
static inline double uitofp_i32_f64(uint32_t x)
{
    return x;
}
static inline double uitofp_i64_f64(uint64_t x)
{
    return x;
}
static inline int8_t fptosi_f64_i8(double x)
{
    return x;
}
static inline int16_t fptosi_f64_i16(double x)
{
    return x;
}
static inline int32_t fptosi_f64_i32(double x)
{
    return x;
}
static inline int64_t fptosi_f64_i64(double x)
{
    return x;
}
static inline uint8_t fptoui_f64_i8(double x)
{
    return x;
}
static inline uint16_t fptoui_f64_i16(double x)
{
    return x;
}
static inline uint32_t fptoui_f64_i32(double x)
{
    return x;
}
static inline uint64_t fptoui_f64_i64(double x)
{
    return x;
}
static inline float fpconv_f32_f32(float x)
{
    return x;
}
static inline double fpconv_f32_f64(float x)
{
    return x;
}
static inline float fpconv_f64_f32(double x)
{
    return x;
}
static inline double fpconv_f64_f64(double x)
{
    return x;
}
static inline float futrts_log32(float x)
{
    return log(x);
}
static inline float futrts_log2_32(float x)
{
    return log2(x);
}
static inline float futrts_log10_32(float x)
{
    return log10(x);
}
static inline float futrts_sqrt32(float x)
{
    return sqrt(x);
}
static inline float futrts_exp32(float x)
{
    return exp(x);
}
static inline float futrts_cos32(float x)
{
    return cos(x);
}
static inline float futrts_sin32(float x)
{
    return sin(x);
}
static inline float futrts_tan32(float x)
{
    return tan(x);
}
static inline float futrts_acos32(float x)
{
    return acos(x);
}
static inline float futrts_asin32(float x)
{
    return asin(x);
}
static inline float futrts_atan32(float x)
{
    return atan(x);
}
static inline float futrts_atan2_32(float x, float y)
{
    return atan2(x, y);
}
static inline float futrts_gamma32(float x)
{
    return tgamma(x);
}
static inline float futrts_lgamma32(float x)
{
    return lgamma(x);
}
static inline char futrts_isnan32(float x)
{
    return isnan(x);
}
static inline char futrts_isinf32(float x)
{
    return isinf(x);
}
static inline int32_t futrts_to_bits32(float x)
{
    union {
        float f;
        int32_t t;
    } p;
    
    p.f = x;
    return p.t;
}
static inline float futrts_from_bits32(int32_t x)
{
    union {
        int32_t f;
        float t;
    } p;
    
    p.f = x;
    return p.t;
}
#ifdef __OPENCL_VERSION__
static inline float fmod32(float x, float y)
{
    return fmod(x, y);
}
static inline float futrts_round32(float x)
{
    return rint(x);
}
static inline float futrts_floor32(float x)
{
    return floor(x);
}
static inline float futrts_ceil32(float x)
{
    return ceil(x);
}
static inline float futrts_lerp32(float v0, float v1, float t)
{
    return mix(v0, v1, t);
}
#else
static inline float fmod32(float x, float y)
{
    return fmodf(x, y);
}
static inline float futrts_round32(float x)
{
    return rintf(x);
}
static inline float futrts_floor32(float x)
{
    return floorf(x);
}
static inline float futrts_ceil32(float x)
{
    return ceilf(x);
}
static inline float futrts_lerp32(float v0, float v1, float t)
{
    return v0 + (v1 - v0) * t;
}
#endif
static inline double futrts_log64(double x)
{
    return log(x);
}
static inline double futrts_log2_64(double x)
{
    return log2(x);
}
static inline double futrts_log10_64(double x)
{
    return log10(x);
}
static inline double futrts_sqrt64(double x)
{
    return sqrt(x);
}
static inline double futrts_exp64(double x)
{
    return exp(x);
}
static inline double futrts_cos64(double x)
{
    return cos(x);
}
static inline double futrts_sin64(double x)
{
    return sin(x);
}
static inline double futrts_tan64(double x)
{
    return tan(x);
}
static inline double futrts_acos64(double x)
{
    return acos(x);
}
static inline double futrts_asin64(double x)
{
    return asin(x);
}
static inline double futrts_atan64(double x)
{
    return atan(x);
}
static inline double futrts_atan2_64(double x, double y)
{
    return atan2(x, y);
}
static inline double futrts_gamma64(double x)
{
    return tgamma(x);
}
static inline double futrts_lgamma64(double x)
{
    return lgamma(x);
}
static inline double futrts_round64(double x)
{
    return rint(x);
}
static inline double futrts_ceil64(double x)
{
    return ceil(x);
}
static inline double futrts_floor64(double x)
{
    return floor(x);
}
static inline char futrts_isnan64(double x)
{
    return isnan(x);
}
static inline char futrts_isinf64(double x)
{
    return isinf(x);
}
static inline int64_t futrts_to_bits64(double x)
{
    union {
        double f;
        int64_t t;
    } p;
    
    p.f = x;
    return p.t;
}
static inline double futrts_from_bits64(int64_t x)
{
    union {
        int64_t f;
        double t;
    } p;
    
    p.f = x;
    return p.t;
}
static inline float fmod64(float x, float y)
{
    return fmod(x, y);
}
#ifdef __OPENCL_VERSION__
static inline double futrts_lerp64(double v0, double v1, double t)
{
    return mix(v0, v1, t);
}
#else
static inline double futrts_lerp64(double v0, double v1, double t)
{
    return v0 + (v1 - v0) * t;
}
#endif
static int futrts_matrixMatrixMul(struct futhark_context *ctx,
                                  struct memblock *out_mem_p_11759,
                                  int32_t *out_out_arrsizze_11760,
                                  int32_t *out_out_arrsizze_11761,
                                  struct memblock A_mem_11670,
                                  struct memblock B_mem_11671, int32_t i_11623,
                                  int32_t j_11624, int32_t j_11625,
                                  int32_t k_11626)
{
    struct memblock out_mem_11753;
    
    out_mem_11753.references = NULL;
    
    int32_t out_arrsizze_11754;
    int32_t out_arrsizze_11755;
    bool dim_zzero_11629 = 0 == j_11625;
    bool dim_zzero_11630 = 0 == k_11626;
    bool old_empty_11631 = dim_zzero_11629 || dim_zzero_11630;
    bool dim_zzero_11632 = 0 == j_11624;
    bool new_empty_11633 = dim_zzero_11630 || dim_zzero_11632;
    bool both_empty_11634 = old_empty_11631 && new_empty_11633;
    bool dim_match_11635 = j_11624 == j_11625;
    bool empty_or_match_11636 = both_empty_11634 || dim_match_11635;
    bool empty_or_match_cert_11637;
    
    if (!empty_or_match_11636) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:23:1-118\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11753, "out_mem_11753") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11623);
    int64_t binop_y_11674 = sext_i32_i64(k_11626);
    int64_t binop_x_11675 = binop_x_11673 * binop_y_11674;
    int64_t bytes_11672 = 4 * binop_x_11675;
    struct memblock mem_11676;
    
    mem_11676.references = NULL;
    if (memblock_alloc(ctx, &mem_11676, bytes_11672, "mem_11676"))
        return 1;
    for (int32_t i_11659 = 0; i_11659 < i_11623; i_11659++) {
        for (int32_t i_11655 = 0; i_11655 < k_11626; i_11655++) {
            float res_11644;
            float redout_11651 = 0.0F;
            
            for (int32_t i_11652 = 0; i_11652 < j_11624; i_11652++) {
                float x_11648 = ((float *) A_mem_11670.mem)[i_11659 * j_11624 +
                                                            i_11652];
                float x_11649 = ((float *) B_mem_11671.mem)[i_11652 * k_11626 +
                                                            i_11655];
                float res_11650 = x_11648 * x_11649;
                float res_11647 = res_11650 + redout_11651;
                float redout_tmp_11758 = res_11647;
                
                redout_11651 = redout_tmp_11758;
            }
            res_11644 = redout_11651;
            ((float *) mem_11676.mem)[i_11659 * k_11626 + i_11655] = res_11644;
        }
    }
    out_arrsizze_11754 = i_11623;
    out_arrsizze_11755 = k_11626;
    if (memblock_set(ctx, &out_mem_11753, &mem_11676, "mem_11676") != 0)
        return 1;
    (*out_mem_p_11759).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11759, &out_mem_11753, "out_mem_11753") !=
        0)
        return 1;
    *out_out_arrsizze_11760 = out_arrsizze_11754;
    *out_out_arrsizze_11761 = out_arrsizze_11755;
    if (memblock_unref(ctx, &mem_11676, "mem_11676") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11753, "out_mem_11753") != 0)
        return 1;
    return 0;
}
static int futrts_vectorMatrixMul(struct futhark_context *ctx,
                                  struct memblock *out_mem_p_11762,
                                  int32_t *out_out_arrsizze_11763,
                                  struct memblock v_mem_11670,
                                  struct memblock M_mem_11671, int32_t i_11598,
                                  int32_t i_11599, int32_t j_11600)
{
    struct memblock out_mem_11749;
    
    out_mem_11749.references = NULL;
    
    int32_t out_arrsizze_11750;
    bool dim_zzero_11603 = 0 == i_11599;
    bool dim_zzero_11604 = 0 == j_11600;
    bool old_empty_11605 = dim_zzero_11603 || dim_zzero_11604;
    bool dim_zzero_11606 = 0 == i_11598;
    bool new_empty_11607 = dim_zzero_11604 || dim_zzero_11606;
    bool both_empty_11608 = old_empty_11605 && new_empty_11607;
    bool dim_match_11609 = i_11598 == i_11599;
    bool empty_or_match_11610 = both_empty_11608 || dim_match_11609;
    bool empty_or_match_cert_11611;
    
    if (!empty_or_match_11610) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:22:1-85\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11749, "out_mem_11749") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(j_11600);
    int64_t bytes_11672 = 4 * binop_x_11673;
    struct memblock mem_11674;
    
    mem_11674.references = NULL;
    if (memblock_alloc(ctx, &mem_11674, bytes_11672, "mem_11674"))
        return 1;
    for (int32_t i_11655 = 0; i_11655 < j_11600; i_11655++) {
        float res_11616;
        float redout_11651 = 0.0F;
        
        for (int32_t i_11652 = 0; i_11652 < i_11598; i_11652++) {
            float x_11620 = ((float *) v_mem_11670.mem)[i_11652];
            float x_11621 = ((float *) M_mem_11671.mem)[i_11652 * j_11600 +
                                                        i_11655];
            float res_11622 = x_11620 * x_11621;
            float res_11619 = res_11622 + redout_11651;
            float redout_tmp_11752 = res_11619;
            
            redout_11651 = redout_tmp_11752;
        }
        res_11616 = redout_11651;
        ((float *) mem_11674.mem)[i_11655] = res_11616;
    }
    out_arrsizze_11750 = j_11600;
    if (memblock_set(ctx, &out_mem_11749, &mem_11674, "mem_11674") != 0)
        return 1;
    (*out_mem_p_11762).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11762, &out_mem_11749, "out_mem_11749") !=
        0)
        return 1;
    *out_out_arrsizze_11763 = out_arrsizze_11750;
    if (memblock_unref(ctx, &mem_11674, "mem_11674") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11749, "out_mem_11749") != 0)
        return 1;
    return 0;
}
static int futrts_matrixMul(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11764,
                            int32_t *out_out_arrsizze_11765,
                            int32_t *out_out_arrsizze_11766,
                            struct memblock A_mem_11670,
                            struct memblock B_mem_11671, int32_t i_11564,
                            int32_t j_11565, int32_t i_11566, int32_t j_11567)
{
    struct memblock out_mem_11744;
    
    out_mem_11744.references = NULL;
    
    int32_t out_arrsizze_11745;
    int32_t out_arrsizze_11746;
    int32_t j_11570 = smax32(j_11565, j_11567);
    bool dim_zzero_11571 = 0 == i_11564;
    bool dim_zzero_11572 = 0 == j_11565;
    bool old_empty_11573 = dim_zzero_11571 || dim_zzero_11572;
    bool dim_zzero_11574 = 0 == j_11570;
    bool new_empty_11575 = dim_zzero_11571 || dim_zzero_11574;
    bool both_empty_11576 = old_empty_11573 && new_empty_11575;
    bool dim_match_11577 = j_11570 == j_11565;
    bool empty_or_match_11578 = both_empty_11576 || dim_match_11577;
    bool empty_or_match_cert_11579;
    
    if (!empty_or_match_11578) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:11:1-86\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11744, "out_mem_11744") != 0)
            return 1;
        return 1;
    }
    
    bool dim_zzero_11581 = 0 == i_11566;
    bool dim_zzero_11582 = 0 == j_11567;
    bool old_empty_11583 = dim_zzero_11581 || dim_zzero_11582;
    bool both_empty_11584 = new_empty_11575 && old_empty_11583;
    bool dim_match_11585 = i_11564 == i_11566;
    bool dim_match_11586 = j_11570 == j_11567;
    bool match_11587 = dim_match_11585 && dim_match_11586;
    bool empty_or_match_11588 = both_empty_11584 || match_11587;
    bool empty_or_match_cert_11589;
    
    if (!empty_or_match_11588) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:11:1-86\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11744, "out_mem_11744") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11564);
    int64_t binop_y_11674 = sext_i32_i64(j_11570);
    int64_t binop_x_11675 = binop_x_11673 * binop_y_11674;
    int64_t bytes_11672 = 4 * binop_x_11675;
    struct memblock mem_11676;
    
    mem_11676.references = NULL;
    if (memblock_alloc(ctx, &mem_11676, bytes_11672, "mem_11676"))
        return 1;
    for (int32_t i_11657 = 0; i_11657 < i_11564; i_11657++) {
        for (int32_t i_11653 = 0; i_11653 < j_11570; i_11653++) {
            float x_11595 = ((float *) A_mem_11670.mem)[i_11657 * j_11565 +
                                                        i_11653];
            float x_11596 = ((float *) B_mem_11671.mem)[i_11657 * j_11567 +
                                                        i_11653];
            float res_11597 = x_11595 * x_11596;
            
            ((float *) mem_11676.mem)[i_11657 * j_11570 + i_11653] = res_11597;
        }
    }
    out_arrsizze_11745 = i_11564;
    out_arrsizze_11746 = j_11570;
    if (memblock_set(ctx, &out_mem_11744, &mem_11676, "mem_11676") != 0)
        return 1;
    (*out_mem_p_11764).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11764, &out_mem_11744, "out_mem_11744") !=
        0)
        return 1;
    *out_out_arrsizze_11765 = out_arrsizze_11745;
    *out_out_arrsizze_11766 = out_arrsizze_11746;
    if (memblock_unref(ctx, &mem_11676, "mem_11676") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11744, "out_mem_11744") != 0)
        return 1;
    return 0;
}
static int futrts_matrixSub(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11767,
                            int32_t *out_out_arrsizze_11768,
                            int32_t *out_out_arrsizze_11769,
                            struct memblock A_mem_11670,
                            struct memblock B_mem_11671, int32_t i_11530,
                            int32_t j_11531, int32_t i_11532, int32_t j_11533)
{
    struct memblock out_mem_11739;
    
    out_mem_11739.references = NULL;
    
    int32_t out_arrsizze_11740;
    int32_t out_arrsizze_11741;
    int32_t j_11536 = smax32(j_11531, j_11533);
    bool dim_zzero_11537 = 0 == i_11530;
    bool dim_zzero_11538 = 0 == j_11531;
    bool old_empty_11539 = dim_zzero_11537 || dim_zzero_11538;
    bool dim_zzero_11540 = 0 == j_11536;
    bool new_empty_11541 = dim_zzero_11537 || dim_zzero_11540;
    bool both_empty_11542 = old_empty_11539 && new_empty_11541;
    bool dim_match_11543 = j_11536 == j_11531;
    bool empty_or_match_11544 = both_empty_11542 || dim_match_11543;
    bool empty_or_match_cert_11545;
    
    if (!empty_or_match_11544) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:10:1-86\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11739, "out_mem_11739") != 0)
            return 1;
        return 1;
    }
    
    bool dim_zzero_11547 = 0 == i_11532;
    bool dim_zzero_11548 = 0 == j_11533;
    bool old_empty_11549 = dim_zzero_11547 || dim_zzero_11548;
    bool both_empty_11550 = new_empty_11541 && old_empty_11549;
    bool dim_match_11551 = i_11530 == i_11532;
    bool dim_match_11552 = j_11536 == j_11533;
    bool match_11553 = dim_match_11551 && dim_match_11552;
    bool empty_or_match_11554 = both_empty_11550 || match_11553;
    bool empty_or_match_cert_11555;
    
    if (!empty_or_match_11554) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:10:1-86\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11739, "out_mem_11739") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11530);
    int64_t binop_y_11674 = sext_i32_i64(j_11536);
    int64_t binop_x_11675 = binop_x_11673 * binop_y_11674;
    int64_t bytes_11672 = 4 * binop_x_11675;
    struct memblock mem_11676;
    
    mem_11676.references = NULL;
    if (memblock_alloc(ctx, &mem_11676, bytes_11672, "mem_11676"))
        return 1;
    for (int32_t i_11657 = 0; i_11657 < i_11530; i_11657++) {
        for (int32_t i_11653 = 0; i_11653 < j_11536; i_11653++) {
            float x_11561 = ((float *) A_mem_11670.mem)[i_11657 * j_11531 +
                                                        i_11653];
            float x_11562 = ((float *) B_mem_11671.mem)[i_11657 * j_11533 +
                                                        i_11653];
            float res_11563 = x_11561 - x_11562;
            
            ((float *) mem_11676.mem)[i_11657 * j_11536 + i_11653] = res_11563;
        }
    }
    out_arrsizze_11740 = i_11530;
    out_arrsizze_11741 = j_11536;
    if (memblock_set(ctx, &out_mem_11739, &mem_11676, "mem_11676") != 0)
        return 1;
    (*out_mem_p_11767).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11767, &out_mem_11739, "out_mem_11739") !=
        0)
        return 1;
    *out_out_arrsizze_11768 = out_arrsizze_11740;
    *out_out_arrsizze_11769 = out_arrsizze_11741;
    if (memblock_unref(ctx, &mem_11676, "mem_11676") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11739, "out_mem_11739") != 0)
        return 1;
    return 0;
}
static int futrts_matrixAdd(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11770,
                            int32_t *out_out_arrsizze_11771,
                            int32_t *out_out_arrsizze_11772,
                            struct memblock A_mem_11670,
                            struct memblock B_mem_11671, int32_t i_11496,
                            int32_t j_11497, int32_t i_11498, int32_t j_11499)
{
    struct memblock out_mem_11734;
    
    out_mem_11734.references = NULL;
    
    int32_t out_arrsizze_11735;
    int32_t out_arrsizze_11736;
    int32_t j_11502 = smax32(j_11497, j_11499);
    bool dim_zzero_11503 = 0 == i_11496;
    bool dim_zzero_11504 = 0 == j_11497;
    bool old_empty_11505 = dim_zzero_11503 || dim_zzero_11504;
    bool dim_zzero_11506 = 0 == j_11502;
    bool new_empty_11507 = dim_zzero_11503 || dim_zzero_11506;
    bool both_empty_11508 = old_empty_11505 && new_empty_11507;
    bool dim_match_11509 = j_11502 == j_11497;
    bool empty_or_match_11510 = both_empty_11508 || dim_match_11509;
    bool empty_or_match_cert_11511;
    
    if (!empty_or_match_11510) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:9:1-86\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11734, "out_mem_11734") != 0)
            return 1;
        return 1;
    }
    
    bool dim_zzero_11513 = 0 == i_11498;
    bool dim_zzero_11514 = 0 == j_11499;
    bool old_empty_11515 = dim_zzero_11513 || dim_zzero_11514;
    bool both_empty_11516 = new_empty_11507 && old_empty_11515;
    bool dim_match_11517 = i_11496 == i_11498;
    bool dim_match_11518 = j_11502 == j_11499;
    bool match_11519 = dim_match_11517 && dim_match_11518;
    bool empty_or_match_11520 = both_empty_11516 || match_11519;
    bool empty_or_match_cert_11521;
    
    if (!empty_or_match_11520) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:9:1-86\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11734, "out_mem_11734") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11496);
    int64_t binop_y_11674 = sext_i32_i64(j_11502);
    int64_t binop_x_11675 = binop_x_11673 * binop_y_11674;
    int64_t bytes_11672 = 4 * binop_x_11675;
    struct memblock mem_11676;
    
    mem_11676.references = NULL;
    if (memblock_alloc(ctx, &mem_11676, bytes_11672, "mem_11676"))
        return 1;
    for (int32_t i_11657 = 0; i_11657 < i_11496; i_11657++) {
        for (int32_t i_11653 = 0; i_11653 < j_11502; i_11653++) {
            float x_11527 = ((float *) A_mem_11670.mem)[i_11657 * j_11497 +
                                                        i_11653];
            float x_11528 = ((float *) B_mem_11671.mem)[i_11657 * j_11499 +
                                                        i_11653];
            float res_11529 = x_11527 + x_11528;
            
            ((float *) mem_11676.mem)[i_11657 * j_11502 + i_11653] = res_11529;
        }
    }
    out_arrsizze_11735 = i_11496;
    out_arrsizze_11736 = j_11502;
    if (memblock_set(ctx, &out_mem_11734, &mem_11676, "mem_11676") != 0)
        return 1;
    (*out_mem_p_11770).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11770, &out_mem_11734, "out_mem_11734") !=
        0)
        return 1;
    *out_out_arrsizze_11771 = out_arrsizze_11735;
    *out_out_arrsizze_11772 = out_arrsizze_11736;
    if (memblock_unref(ctx, &mem_11676, "mem_11676") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11734, "out_mem_11734") != 0)
        return 1;
    return 0;
}
static int futrts_matrixVectorMul(struct futhark_context *ctx,
                                  struct memblock *out_mem_p_11773,
                                  int32_t *out_out_arrsizze_11774,
                                  struct memblock M_mem_11670,
                                  struct memblock v_mem_11671, int32_t i_11475,
                                  int32_t j_11476, int32_t sizze_11477)
{
    struct memblock out_mem_11730;
    
    out_mem_11730.references = NULL;
    
    int32_t out_arrsizze_11731;
    bool dim_zzero_11480 = 0 == j_11476;
    bool dim_zzero_11481 = 0 == sizze_11477;
    bool both_empty_11482 = dim_zzero_11480 && dim_zzero_11481;
    bool dim_match_11483 = sizze_11477 == j_11476;
    bool empty_or_match_11484 = both_empty_11482 || dim_match_11483;
    bool empty_or_match_cert_11485;
    
    if (!empty_or_match_11484) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " |-> algebra.fut:21:1-71\n |-> algebra.fut:21:59-71\n `-> unknown location\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11730, "out_mem_11730") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11475);
    int64_t bytes_11672 = 4 * binop_x_11673;
    struct memblock mem_11674;
    
    mem_11674.references = NULL;
    if (memblock_alloc(ctx, &mem_11674, bytes_11672, "mem_11674"))
        return 1;
    for (int32_t i_11655 = 0; i_11655 < i_11475; i_11655++) {
        float res_11489;
        float redout_11651 = 0.0F;
        
        for (int32_t i_11652 = 0; i_11652 < sizze_11477; i_11652++) {
            float x_11493 = ((float *) v_mem_11671.mem)[i_11652];
            float x_11494 = ((float *) M_mem_11670.mem)[i_11655 * j_11476 +
                                                        i_11652];
            float res_11495 = x_11493 * x_11494;
            float res_11492 = res_11495 + redout_11651;
            float redout_tmp_11733 = res_11492;
            
            redout_11651 = redout_tmp_11733;
        }
        res_11489 = redout_11651;
        ((float *) mem_11674.mem)[i_11655] = res_11489;
    }
    out_arrsizze_11731 = i_11475;
    if (memblock_set(ctx, &out_mem_11730, &mem_11674, "mem_11674") != 0)
        return 1;
    (*out_mem_p_11773).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11773, &out_mem_11730, "out_mem_11730") !=
        0)
        return 1;
    *out_out_arrsizze_11774 = out_arrsizze_11731;
    if (memblock_unref(ctx, &mem_11674, "mem_11674") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11730, "out_mem_11730") != 0)
        return 1;
    return 0;
}
static int futrts_normalizzeV(struct futhark_context *ctx,
                              struct memblock *out_mem_p_11775,
                              int32_t *out_out_arrsizze_11776,
                              struct memblock v_mem_11670, int32_t sizze_11462)
{
    struct memblock out_mem_11726;
    
    out_mem_11726.references = NULL;
    
    int32_t out_arrsizze_11727;
    float res_11464;
    float redout_11651 = 0.0F;
    
    for (int32_t i_11652 = 0; i_11652 < sizze_11462; i_11652++) {
        float x_11468 = ((float *) v_mem_11670.mem)[i_11652];
        float res_11469 = x_11468 * x_11468;
        float res_11467 = res_11469 + redout_11651;
        float redout_tmp_11728 = res_11467;
        
        redout_11651 = redout_tmp_11728;
    }
    res_11464 = redout_11651;
    
    float res_11470;
    
    res_11470 = futrts_sqrt32(res_11464);
    
    float f_11471 = 1.0F / res_11470;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11462);
    int64_t bytes_11671 = 4 * binop_x_11672;
    struct memblock mem_11673;
    
    mem_11673.references = NULL;
    if (memblock_alloc(ctx, &mem_11673, bytes_11671, "mem_11673"))
        return 1;
    for (int32_t i_11655 = 0; i_11655 < sizze_11462; i_11655++) {
        float x_11473 = ((float *) v_mem_11670.mem)[i_11655];
        float res_11474 = f_11471 * x_11473;
        
        ((float *) mem_11673.mem)[i_11655] = res_11474;
    }
    out_arrsizze_11727 = sizze_11462;
    if (memblock_set(ctx, &out_mem_11726, &mem_11673, "mem_11673") != 0)
        return 1;
    (*out_mem_p_11775).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11775, &out_mem_11726, "out_mem_11726") !=
        0)
        return 1;
    *out_out_arrsizze_11776 = out_arrsizze_11727;
    if (memblock_unref(ctx, &mem_11673, "mem_11673") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11726, "out_mem_11726") != 0)
        return 1;
    return 0;
}
static int futrts_vectorNorm(struct futhark_context *ctx,
                             float *out_scalar_out_11777,
                             struct memblock v_mem_11670, int32_t sizze_11453)
{
    float scalar_out_11724;
    float res_11455;
    float redout_11651 = 0.0F;
    
    for (int32_t i_11652 = 0; i_11652 < sizze_11453; i_11652++) {
        float x_11459 = ((float *) v_mem_11670.mem)[i_11652];
        float res_11460 = x_11459 * x_11459;
        float res_11458 = res_11460 + redout_11651;
        float redout_tmp_11725 = res_11458;
        
        redout_11651 = redout_tmp_11725;
    }
    res_11455 = redout_11651;
    
    float res_11461;
    
    res_11461 = futrts_sqrt32(res_11455);
    scalar_out_11724 = res_11461;
    *out_scalar_out_11777 = scalar_out_11724;
    return 0;
}
static int futrts_dot(struct futhark_context *ctx, float *out_scalar_out_11778,
                      struct memblock a_mem_11670, struct memblock b_mem_11671,
                      int32_t i_11435, int32_t i_11436)
{
    float scalar_out_11722;
    bool dim_zzero_11439 = 0 == i_11436;
    bool dim_zzero_11440 = 0 == i_11435;
    bool both_empty_11441 = dim_zzero_11439 && dim_zzero_11440;
    bool dim_match_11442 = i_11435 == i_11436;
    bool empty_or_match_11443 = both_empty_11441 || dim_match_11442;
    bool empty_or_match_cert_11444;
    
    if (!empty_or_match_11443) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:16:1-75\n",
                               "function arguments of wrong shape");
        return 1;
    }
    
    float res_11446;
    float redout_11651 = 0.0F;
    
    for (int32_t i_11652 = 0; i_11652 < i_11435; i_11652++) {
        float x_11450 = ((float *) a_mem_11670.mem)[i_11652];
        float x_11451 = ((float *) b_mem_11671.mem)[i_11652];
        float res_11452 = x_11450 * x_11451;
        float res_11449 = res_11452 + redout_11651;
        float redout_tmp_11723 = res_11449;
        
        redout_11651 = redout_tmp_11723;
    }
    res_11446 = redout_11651;
    scalar_out_11722 = res_11446;
    *out_scalar_out_11778 = scalar_out_11722;
    return 0;
}
static int futrts_matrixSgn(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11779,
                            int32_t *out_out_arrsizze_11780,
                            int32_t *out_out_arrsizze_11781,
                            struct memblock x_mem_11670, int32_t sizze_11424,
                            int32_t sizze_11425)
{
    struct memblock out_mem_11717;
    
    out_mem_11717.references = NULL;
    
    int32_t out_arrsizze_11718;
    int32_t out_arrsizze_11719;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11424);
    int64_t binop_y_11673 = sext_i32_i64(sizze_11425);
    int64_t binop_x_11674 = binop_x_11672 * binop_y_11673;
    int64_t bytes_11671 = 4 * binop_x_11674;
    struct memblock mem_11675;
    
    mem_11675.references = NULL;
    if (memblock_alloc(ctx, &mem_11675, bytes_11671, "mem_11675"))
        return 1;
    for (int32_t i_11657 = 0; i_11657 < sizze_11424; i_11657++) {
        for (int32_t i_11653 = 0; i_11653 < sizze_11425; i_11653++) {
            float x_11430 = ((float *) x_mem_11670.mem)[i_11657 * sizze_11425 +
                                                        i_11653];
            bool res_11431 = x_11430 < 0.0F;
            float res_11432;
            
            if (res_11431) {
                res_11432 = -1.0F;
            } else {
                bool res_11433 = x_11430 == 0.0F;
                float res_11434;
                
                if (res_11433) {
                    res_11434 = 0.0F;
                } else {
                    res_11434 = 1.0F;
                }
                res_11432 = res_11434;
            }
            ((float *) mem_11675.mem)[i_11657 * sizze_11425 + i_11653] =
                res_11432;
        }
    }
    out_arrsizze_11718 = sizze_11424;
    out_arrsizze_11719 = sizze_11425;
    if (memblock_set(ctx, &out_mem_11717, &mem_11675, "mem_11675") != 0)
        return 1;
    (*out_mem_p_11779).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11779, &out_mem_11717, "out_mem_11717") !=
        0)
        return 1;
    *out_out_arrsizze_11780 = out_arrsizze_11718;
    *out_out_arrsizze_11781 = out_arrsizze_11719;
    if (memblock_unref(ctx, &mem_11675, "mem_11675") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11717, "out_mem_11717") != 0)
        return 1;
    return 0;
}
static int futrts_scaleM(struct futhark_context *ctx,
                         struct memblock *out_mem_p_11782,
                         int32_t *out_out_arrsizze_11783,
                         int32_t *out_out_arrsizze_11784,
                         struct memblock x_mem_11670, int32_t sizze_11415,
                         int32_t sizze_11416, float f_11417)
{
    struct memblock out_mem_11712;
    
    out_mem_11712.references = NULL;
    
    int32_t out_arrsizze_11713;
    int32_t out_arrsizze_11714;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11415);
    int64_t binop_y_11673 = sext_i32_i64(sizze_11416);
    int64_t binop_x_11674 = binop_x_11672 * binop_y_11673;
    int64_t bytes_11671 = 4 * binop_x_11674;
    struct memblock mem_11675;
    
    mem_11675.references = NULL;
    if (memblock_alloc(ctx, &mem_11675, bytes_11671, "mem_11675"))
        return 1;
    for (int32_t i_11657 = 0; i_11657 < sizze_11415; i_11657++) {
        for (int32_t i_11653 = 0; i_11653 < sizze_11416; i_11653++) {
            float x_11422 = ((float *) x_mem_11670.mem)[i_11657 * sizze_11416 +
                                                        i_11653];
            float res_11423 = f_11417 * x_11422;
            
            ((float *) mem_11675.mem)[i_11657 * sizze_11416 + i_11653] =
                res_11423;
        }
    }
    out_arrsizze_11713 = sizze_11415;
    out_arrsizze_11714 = sizze_11416;
    if (memblock_set(ctx, &out_mem_11712, &mem_11675, "mem_11675") != 0)
        return 1;
    (*out_mem_p_11782).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11782, &out_mem_11712, "out_mem_11712") !=
        0)
        return 1;
    *out_out_arrsizze_11783 = out_arrsizze_11713;
    *out_out_arrsizze_11784 = out_arrsizze_11714;
    if (memblock_unref(ctx, &mem_11675, "mem_11675") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11712, "out_mem_11712") != 0)
        return 1;
    return 0;
}
static int futrts_matrixAbs(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11785,
                            int32_t *out_out_arrsizze_11786,
                            int32_t *out_out_arrsizze_11787,
                            struct memblock x_mem_11670, int32_t sizze_11407,
                            int32_t sizze_11408)
{
    struct memblock out_mem_11707;
    
    out_mem_11707.references = NULL;
    
    int32_t out_arrsizze_11708;
    int32_t out_arrsizze_11709;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11407);
    int64_t binop_y_11673 = sext_i32_i64(sizze_11408);
    int64_t binop_x_11674 = binop_x_11672 * binop_y_11673;
    int64_t bytes_11671 = 4 * binop_x_11674;
    struct memblock mem_11675;
    
    mem_11675.references = NULL;
    if (memblock_alloc(ctx, &mem_11675, bytes_11671, "mem_11675"))
        return 1;
    for (int32_t i_11657 = 0; i_11657 < sizze_11407; i_11657++) {
        for (int32_t i_11653 = 0; i_11653 < sizze_11408; i_11653++) {
            float x_11413 = ((float *) x_mem_11670.mem)[i_11657 * sizze_11408 +
                                                        i_11653];
            float res_11414 = (float) fabs(x_11413);
            
            ((float *) mem_11675.mem)[i_11657 * sizze_11408 + i_11653] =
                res_11414;
        }
    }
    out_arrsizze_11708 = sizze_11407;
    out_arrsizze_11709 = sizze_11408;
    if (memblock_set(ctx, &out_mem_11707, &mem_11675, "mem_11675") != 0)
        return 1;
    (*out_mem_p_11785).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11785, &out_mem_11707, "out_mem_11707") !=
        0)
        return 1;
    *out_out_arrsizze_11786 = out_arrsizze_11708;
    *out_out_arrsizze_11787 = out_arrsizze_11709;
    if (memblock_unref(ctx, &mem_11675, "mem_11675") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11707, "out_mem_11707") != 0)
        return 1;
    return 0;
}
static int futrts_vectorMul(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11788,
                            int32_t *out_out_arrsizze_11789,
                            struct memblock a_mem_11670,
                            struct memblock b_mem_11671, int32_t i_11392,
                            int32_t i_11393)
{
    struct memblock out_mem_11704;
    
    out_mem_11704.references = NULL;
    
    int32_t out_arrsizze_11705;
    bool dim_zzero_11396 = 0 == i_11393;
    bool dim_zzero_11397 = 0 == i_11392;
    bool both_empty_11398 = dim_zzero_11396 && dim_zzero_11397;
    bool dim_match_11399 = i_11392 == i_11393;
    bool empty_or_match_11400 = both_empty_11398 || dim_match_11399;
    bool empty_or_match_cert_11401;
    
    if (!empty_or_match_11400) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:4:1-67\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11704, "out_mem_11704") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11392);
    int64_t bytes_11672 = 4 * binop_x_11673;
    struct memblock mem_11674;
    
    mem_11674.references = NULL;
    if (memblock_alloc(ctx, &mem_11674, bytes_11672, "mem_11674"))
        return 1;
    for (int32_t i_11653 = 0; i_11653 < i_11392; i_11653++) {
        float x_11404 = ((float *) a_mem_11670.mem)[i_11653];
        float x_11405 = ((float *) b_mem_11671.mem)[i_11653];
        float res_11406 = x_11404 * x_11405;
        
        ((float *) mem_11674.mem)[i_11653] = res_11406;
    }
    out_arrsizze_11705 = i_11392;
    if (memblock_set(ctx, &out_mem_11704, &mem_11674, "mem_11674") != 0)
        return 1;
    (*out_mem_p_11788).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11788, &out_mem_11704, "out_mem_11704") !=
        0)
        return 1;
    *out_out_arrsizze_11789 = out_arrsizze_11705;
    if (memblock_unref(ctx, &mem_11674, "mem_11674") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11704, "out_mem_11704") != 0)
        return 1;
    return 0;
}
static int futrts_vectorSub(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11790,
                            int32_t *out_out_arrsizze_11791,
                            struct memblock a_mem_11670,
                            struct memblock b_mem_11671, int32_t i_11377,
                            int32_t i_11378)
{
    struct memblock out_mem_11701;
    
    out_mem_11701.references = NULL;
    
    int32_t out_arrsizze_11702;
    bool dim_zzero_11381 = 0 == i_11378;
    bool dim_zzero_11382 = 0 == i_11377;
    bool both_empty_11383 = dim_zzero_11381 && dim_zzero_11382;
    bool dim_match_11384 = i_11377 == i_11378;
    bool empty_or_match_11385 = both_empty_11383 || dim_match_11384;
    bool empty_or_match_cert_11386;
    
    if (!empty_or_match_11385) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:3:1-67\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11701, "out_mem_11701") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11377);
    int64_t bytes_11672 = 4 * binop_x_11673;
    struct memblock mem_11674;
    
    mem_11674.references = NULL;
    if (memblock_alloc(ctx, &mem_11674, bytes_11672, "mem_11674"))
        return 1;
    for (int32_t i_11653 = 0; i_11653 < i_11377; i_11653++) {
        float x_11389 = ((float *) a_mem_11670.mem)[i_11653];
        float x_11390 = ((float *) b_mem_11671.mem)[i_11653];
        float res_11391 = x_11389 - x_11390;
        
        ((float *) mem_11674.mem)[i_11653] = res_11391;
    }
    out_arrsizze_11702 = i_11377;
    if (memblock_set(ctx, &out_mem_11701, &mem_11674, "mem_11674") != 0)
        return 1;
    (*out_mem_p_11790).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11790, &out_mem_11701, "out_mem_11701") !=
        0)
        return 1;
    *out_out_arrsizze_11791 = out_arrsizze_11702;
    if (memblock_unref(ctx, &mem_11674, "mem_11674") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11701, "out_mem_11701") != 0)
        return 1;
    return 0;
}
static int futrts_vectorAdd(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11792,
                            int32_t *out_out_arrsizze_11793,
                            struct memblock a_mem_11670,
                            struct memblock b_mem_11671, int32_t i_11362,
                            int32_t i_11363)
{
    struct memblock out_mem_11698;
    
    out_mem_11698.references = NULL;
    
    int32_t out_arrsizze_11699;
    bool dim_zzero_11366 = 0 == i_11363;
    bool dim_zzero_11367 = 0 == i_11362;
    bool both_empty_11368 = dim_zzero_11366 && dim_zzero_11367;
    bool dim_match_11369 = i_11362 == i_11363;
    bool empty_or_match_11370 = both_empty_11368 || dim_match_11369;
    bool empty_or_match_cert_11371;
    
    if (!empty_or_match_11370) {
        ctx->error = msgprintf("Error at\n%s\n%s\n",
                               " `-> algebra.fut:2:1-67\n",
                               "function arguments of wrong shape");
        if (memblock_unref(ctx, &out_mem_11698, "out_mem_11698") != 0)
            return 1;
        return 1;
    }
    
    int64_t binop_x_11673 = sext_i32_i64(i_11362);
    int64_t bytes_11672 = 4 * binop_x_11673;
    struct memblock mem_11674;
    
    mem_11674.references = NULL;
    if (memblock_alloc(ctx, &mem_11674, bytes_11672, "mem_11674"))
        return 1;
    for (int32_t i_11653 = 0; i_11653 < i_11362; i_11653++) {
        float x_11374 = ((float *) a_mem_11670.mem)[i_11653];
        float x_11375 = ((float *) b_mem_11671.mem)[i_11653];
        float res_11376 = x_11374 + x_11375;
        
        ((float *) mem_11674.mem)[i_11653] = res_11376;
    }
    out_arrsizze_11699 = i_11362;
    if (memblock_set(ctx, &out_mem_11698, &mem_11674, "mem_11674") != 0)
        return 1;
    (*out_mem_p_11792).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11792, &out_mem_11698, "out_mem_11698") !=
        0)
        return 1;
    *out_out_arrsizze_11793 = out_arrsizze_11699;
    if (memblock_unref(ctx, &mem_11674, "mem_11674") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11698, "out_mem_11698") != 0)
        return 1;
    return 0;
}
static int futrts_vectorSgn(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11794,
                            int32_t *out_out_arrsizze_11795,
                            struct memblock x_mem_11670, int32_t sizze_11354)
{
    struct memblock out_mem_11695;
    
    out_mem_11695.references = NULL;
    
    int32_t out_arrsizze_11696;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11354);
    int64_t bytes_11671 = 4 * binop_x_11672;
    struct memblock mem_11673;
    
    mem_11673.references = NULL;
    if (memblock_alloc(ctx, &mem_11673, bytes_11671, "mem_11673"))
        return 1;
    for (int32_t i_11653 = 0; i_11653 < sizze_11354; i_11653++) {
        float x_11357 = ((float *) x_mem_11670.mem)[i_11653];
        bool res_11358 = x_11357 < 0.0F;
        float res_11359;
        
        if (res_11358) {
            res_11359 = -1.0F;
        } else {
            bool res_11360 = x_11357 == 0.0F;
            float res_11361;
            
            if (res_11360) {
                res_11361 = 0.0F;
            } else {
                res_11361 = 1.0F;
            }
            res_11359 = res_11361;
        }
        ((float *) mem_11673.mem)[i_11653] = res_11359;
    }
    out_arrsizze_11696 = sizze_11354;
    if (memblock_set(ctx, &out_mem_11695, &mem_11673, "mem_11673") != 0)
        return 1;
    (*out_mem_p_11794).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11794, &out_mem_11695, "out_mem_11695") !=
        0)
        return 1;
    *out_out_arrsizze_11795 = out_arrsizze_11696;
    if (memblock_unref(ctx, &mem_11673, "mem_11673") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11695, "out_mem_11695") != 0)
        return 1;
    return 0;
}
static int futrts_scaleV(struct futhark_context *ctx,
                         struct memblock *out_mem_p_11796,
                         int32_t *out_out_arrsizze_11797,
                         struct memblock x_mem_11670, int32_t sizze_11348,
                         float f_11349)
{
    struct memblock out_mem_11692;
    
    out_mem_11692.references = NULL;
    
    int32_t out_arrsizze_11693;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11348);
    int64_t bytes_11671 = 4 * binop_x_11672;
    struct memblock mem_11673;
    
    mem_11673.references = NULL;
    if (memblock_alloc(ctx, &mem_11673, bytes_11671, "mem_11673"))
        return 1;
    for (int32_t i_11653 = 0; i_11653 < sizze_11348; i_11653++) {
        float x_11352 = ((float *) x_mem_11670.mem)[i_11653];
        float res_11353 = f_11349 * x_11352;
        
        ((float *) mem_11673.mem)[i_11653] = res_11353;
    }
    out_arrsizze_11693 = sizze_11348;
    if (memblock_set(ctx, &out_mem_11692, &mem_11673, "mem_11673") != 0)
        return 1;
    (*out_mem_p_11796).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11796, &out_mem_11692, "out_mem_11692") !=
        0)
        return 1;
    *out_out_arrsizze_11797 = out_arrsizze_11693;
    if (memblock_unref(ctx, &mem_11673, "mem_11673") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11692, "out_mem_11692") != 0)
        return 1;
    return 0;
}
static int futrts_vectorAbs(struct futhark_context *ctx,
                            struct memblock *out_mem_p_11798,
                            int32_t *out_out_arrsizze_11799,
                            struct memblock x_mem_11670, int32_t sizze_11343)
{
    struct memblock out_mem_11689;
    
    out_mem_11689.references = NULL;
    
    int32_t out_arrsizze_11690;
    int64_t binop_x_11672 = sext_i32_i64(sizze_11343);
    int64_t bytes_11671 = 4 * binop_x_11672;
    struct memblock mem_11673;
    
    mem_11673.references = NULL;
    if (memblock_alloc(ctx, &mem_11673, bytes_11671, "mem_11673"))
        return 1;
    for (int32_t i_11653 = 0; i_11653 < sizze_11343; i_11653++) {
        float x_11346 = ((float *) x_mem_11670.mem)[i_11653];
        float res_11347 = (float) fabs(x_11346);
        
        ((float *) mem_11673.mem)[i_11653] = res_11347;
    }
    out_arrsizze_11690 = sizze_11343;
    if (memblock_set(ctx, &out_mem_11689, &mem_11673, "mem_11673") != 0)
        return 1;
    (*out_mem_p_11798).references = NULL;
    if (memblock_set(ctx, &*out_mem_p_11798, &out_mem_11689, "out_mem_11689") !=
        0)
        return 1;
    *out_out_arrsizze_11799 = out_arrsizze_11690;
    if (memblock_unref(ctx, &mem_11673, "mem_11673") != 0)
        return 1;
    if (memblock_unref(ctx, &out_mem_11689, "out_mem_11689") != 0)
        return 1;
    return 0;
}
struct futhark_f32_1d {
    struct memblock mem;
    int64_t shape[1];
} ;
struct futhark_f32_1d *futhark_new_f32_1d(struct futhark_context *ctx,
                                          float *data, int64_t dim0)
{
    struct futhark_f32_1d *bad = NULL;
    struct futhark_f32_1d *arr =
                          (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d));
    
    if (arr == NULL)
        return bad;
    lock_lock(&ctx->lock);
    arr->mem.references = NULL;
    if (memblock_alloc(ctx, &arr->mem, dim0 * sizeof(float), "arr->mem"))
        return NULL;
    arr->shape[0] = dim0;
    memmove(arr->mem.mem + 0, data + 0, dim0 * sizeof(float));
    lock_unlock(&ctx->lock);
    return arr;
}
struct futhark_f32_1d *futhark_new_raw_f32_1d(struct futhark_context *ctx,
                                              char *data, int offset,
                                              int64_t dim0)
{
    struct futhark_f32_1d *bad = NULL;
    struct futhark_f32_1d *arr =
                          (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d));
    
    if (arr == NULL)
        return bad;
    lock_lock(&ctx->lock);
    arr->mem.references = NULL;
    if (memblock_alloc(ctx, &arr->mem, dim0 * sizeof(float), "arr->mem"))
        return NULL;
    arr->shape[0] = dim0;
    memmove(arr->mem.mem + 0, data + offset, dim0 * sizeof(float));
    lock_unlock(&ctx->lock);
    return arr;
}
int futhark_free_f32_1d(struct futhark_context *ctx, struct futhark_f32_1d *arr)
{
    lock_lock(&ctx->lock);
    if (memblock_unref(ctx, &arr->mem, "arr->mem") != 0)
        return 1;
    lock_unlock(&ctx->lock);
    free(arr);
    return 0;
}
int futhark_values_f32_1d(struct futhark_context *ctx,
                          struct futhark_f32_1d *arr, float *data)
{
    lock_lock(&ctx->lock);
    memmove(data + 0, arr->mem.mem + 0, arr->shape[0] * sizeof(float));
    lock_unlock(&ctx->lock);
    return 0;
}
char *futhark_values_raw_f32_1d(struct futhark_context *ctx,
                                struct futhark_f32_1d *arr)
{
    (void) ctx;
    return arr->mem.mem;
}
int64_t *futhark_shape_f32_1d(struct futhark_context *ctx,
                              struct futhark_f32_1d *arr)
{
    (void) ctx;
    return arr->shape;
}
struct futhark_f32_2d {
    struct memblock mem;
    int64_t shape[2];
} ;
struct futhark_f32_2d *futhark_new_f32_2d(struct futhark_context *ctx,
                                          float *data, int64_t dim0,
                                          int64_t dim1)
{
    struct futhark_f32_2d *bad = NULL;
    struct futhark_f32_2d *arr =
                          (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d));
    
    if (arr == NULL)
        return bad;
    lock_lock(&ctx->lock);
    arr->mem.references = NULL;
    if (memblock_alloc(ctx, &arr->mem, dim0 * dim1 * sizeof(float), "arr->mem"))
        return NULL;
    arr->shape[0] = dim0;
    arr->shape[1] = dim1;
    memmove(arr->mem.mem + 0, data + 0, dim0 * dim1 * sizeof(float));
    lock_unlock(&ctx->lock);
    return arr;
}
struct futhark_f32_2d *futhark_new_raw_f32_2d(struct futhark_context *ctx,
                                              char *data, int offset,
                                              int64_t dim0, int64_t dim1)
{
    struct futhark_f32_2d *bad = NULL;
    struct futhark_f32_2d *arr =
                          (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d));
    
    if (arr == NULL)
        return bad;
    lock_lock(&ctx->lock);
    arr->mem.references = NULL;
    if (memblock_alloc(ctx, &arr->mem, dim0 * dim1 * sizeof(float), "arr->mem"))
        return NULL;
    arr->shape[0] = dim0;
    arr->shape[1] = dim1;
    memmove(arr->mem.mem + 0, data + offset, dim0 * dim1 * sizeof(float));
    lock_unlock(&ctx->lock);
    return arr;
}
int futhark_free_f32_2d(struct futhark_context *ctx, struct futhark_f32_2d *arr)
{
    lock_lock(&ctx->lock);
    if (memblock_unref(ctx, &arr->mem, "arr->mem") != 0)
        return 1;
    lock_unlock(&ctx->lock);
    free(arr);
    return 0;
}
int futhark_values_f32_2d(struct futhark_context *ctx,
                          struct futhark_f32_2d *arr, float *data)
{
    lock_lock(&ctx->lock);
    memmove(data + 0, arr->mem.mem + 0, arr->shape[0] * arr->shape[1] *
            sizeof(float));
    lock_unlock(&ctx->lock);
    return 0;
}
char *futhark_values_raw_f32_2d(struct futhark_context *ctx,
                                struct futhark_f32_2d *arr)
{
    (void) ctx;
    return arr->mem.mem;
}
int64_t *futhark_shape_f32_2d(struct futhark_context *ctx,
                              struct futhark_f32_2d *arr)
{
    (void) ctx;
    return arr->shape;
}
int futhark_entry_matrixMatrixMul(struct futhark_context *ctx,
                                  struct futhark_f32_2d **out0, const
                                  struct futhark_f32_2d *in0, const
                                  struct futhark_f32_2d *in1)
{
    struct memblock A_mem_11670;
    
    A_mem_11670.references = NULL;
    
    struct memblock B_mem_11671;
    
    B_mem_11671.references = NULL;
    
    int32_t i_11623;
    int32_t j_11624;
    int32_t j_11625;
    int32_t k_11626;
    struct memblock out_mem_11753;
    
    out_mem_11753.references = NULL;
    
    int32_t out_arrsizze_11754;
    int32_t out_arrsizze_11755;
    
    lock_lock(&ctx->lock);
    A_mem_11670 = in0->mem;
    i_11623 = in0->shape[0];
    j_11624 = in0->shape[1];
    B_mem_11671 = in1->mem;
    j_11625 = in1->shape[0];
    k_11626 = in1->shape[1];
    
    int ret = futrts_matrixMatrixMul(ctx, &out_mem_11753, &out_arrsizze_11754,
                                     &out_arrsizze_11755, A_mem_11670,
                                     B_mem_11671, i_11623, j_11624, j_11625,
                                     k_11626);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11753;
        (*out0)->shape[0] = out_arrsizze_11754;
        (*out0)->shape[1] = out_arrsizze_11755;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorMatrixMul(struct futhark_context *ctx,
                                  struct futhark_f32_1d **out0, const
                                  struct futhark_f32_1d *in0, const
                                  struct futhark_f32_2d *in1)
{
    struct memblock v_mem_11670;
    
    v_mem_11670.references = NULL;
    
    struct memblock M_mem_11671;
    
    M_mem_11671.references = NULL;
    
    int32_t i_11598;
    int32_t i_11599;
    int32_t j_11600;
    struct memblock out_mem_11749;
    
    out_mem_11749.references = NULL;
    
    int32_t out_arrsizze_11750;
    
    lock_lock(&ctx->lock);
    v_mem_11670 = in0->mem;
    i_11598 = in0->shape[0];
    M_mem_11671 = in1->mem;
    i_11599 = in1->shape[0];
    j_11600 = in1->shape[1];
    
    int ret = futrts_vectorMatrixMul(ctx, &out_mem_11749, &out_arrsizze_11750,
                                     v_mem_11670, M_mem_11671, i_11598, i_11599,
                                     j_11600);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11749;
        (*out0)->shape[0] = out_arrsizze_11750;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_matrixMul(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0, const
                            struct futhark_f32_2d *in1)
{
    struct memblock A_mem_11670;
    
    A_mem_11670.references = NULL;
    
    struct memblock B_mem_11671;
    
    B_mem_11671.references = NULL;
    
    int32_t i_11564;
    int32_t j_11565;
    int32_t i_11566;
    int32_t j_11567;
    struct memblock out_mem_11744;
    
    out_mem_11744.references = NULL;
    
    int32_t out_arrsizze_11745;
    int32_t out_arrsizze_11746;
    
    lock_lock(&ctx->lock);
    A_mem_11670 = in0->mem;
    i_11564 = in0->shape[0];
    j_11565 = in0->shape[1];
    B_mem_11671 = in1->mem;
    i_11566 = in1->shape[0];
    j_11567 = in1->shape[1];
    
    int ret = futrts_matrixMul(ctx, &out_mem_11744, &out_arrsizze_11745,
                               &out_arrsizze_11746, A_mem_11670, B_mem_11671,
                               i_11564, j_11565, i_11566, j_11567);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11744;
        (*out0)->shape[0] = out_arrsizze_11745;
        (*out0)->shape[1] = out_arrsizze_11746;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_matrixSub(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0, const
                            struct futhark_f32_2d *in1)
{
    struct memblock A_mem_11670;
    
    A_mem_11670.references = NULL;
    
    struct memblock B_mem_11671;
    
    B_mem_11671.references = NULL;
    
    int32_t i_11530;
    int32_t j_11531;
    int32_t i_11532;
    int32_t j_11533;
    struct memblock out_mem_11739;
    
    out_mem_11739.references = NULL;
    
    int32_t out_arrsizze_11740;
    int32_t out_arrsizze_11741;
    
    lock_lock(&ctx->lock);
    A_mem_11670 = in0->mem;
    i_11530 = in0->shape[0];
    j_11531 = in0->shape[1];
    B_mem_11671 = in1->mem;
    i_11532 = in1->shape[0];
    j_11533 = in1->shape[1];
    
    int ret = futrts_matrixSub(ctx, &out_mem_11739, &out_arrsizze_11740,
                               &out_arrsizze_11741, A_mem_11670, B_mem_11671,
                               i_11530, j_11531, i_11532, j_11533);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11739;
        (*out0)->shape[0] = out_arrsizze_11740;
        (*out0)->shape[1] = out_arrsizze_11741;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_matrixAdd(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0, const
                            struct futhark_f32_2d *in1)
{
    struct memblock A_mem_11670;
    
    A_mem_11670.references = NULL;
    
    struct memblock B_mem_11671;
    
    B_mem_11671.references = NULL;
    
    int32_t i_11496;
    int32_t j_11497;
    int32_t i_11498;
    int32_t j_11499;
    struct memblock out_mem_11734;
    
    out_mem_11734.references = NULL;
    
    int32_t out_arrsizze_11735;
    int32_t out_arrsizze_11736;
    
    lock_lock(&ctx->lock);
    A_mem_11670 = in0->mem;
    i_11496 = in0->shape[0];
    j_11497 = in0->shape[1];
    B_mem_11671 = in1->mem;
    i_11498 = in1->shape[0];
    j_11499 = in1->shape[1];
    
    int ret = futrts_matrixAdd(ctx, &out_mem_11734, &out_arrsizze_11735,
                               &out_arrsizze_11736, A_mem_11670, B_mem_11671,
                               i_11496, j_11497, i_11498, j_11499);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11734;
        (*out0)->shape[0] = out_arrsizze_11735;
        (*out0)->shape[1] = out_arrsizze_11736;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_matrixVectorMul(struct futhark_context *ctx,
                                  struct futhark_f32_1d **out0, const
                                  struct futhark_f32_2d *in0, const
                                  struct futhark_f32_1d *in1)
{
    struct memblock M_mem_11670;
    
    M_mem_11670.references = NULL;
    
    struct memblock v_mem_11671;
    
    v_mem_11671.references = NULL;
    
    int32_t i_11475;
    int32_t j_11476;
    int32_t sizze_11477;
    struct memblock out_mem_11730;
    
    out_mem_11730.references = NULL;
    
    int32_t out_arrsizze_11731;
    
    lock_lock(&ctx->lock);
    M_mem_11670 = in0->mem;
    i_11475 = in0->shape[0];
    j_11476 = in0->shape[1];
    v_mem_11671 = in1->mem;
    sizze_11477 = in1->shape[0];
    
    int ret = futrts_matrixVectorMul(ctx, &out_mem_11730, &out_arrsizze_11731,
                                     M_mem_11670, v_mem_11671, i_11475, j_11476,
                                     sizze_11477);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11730;
        (*out0)->shape[0] = out_arrsizze_11731;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_normalizeV(struct futhark_context *ctx,
                             struct futhark_f32_1d **out0, const
                             struct futhark_f32_1d *in0)
{
    struct memblock v_mem_11670;
    
    v_mem_11670.references = NULL;
    
    int32_t sizze_11462;
    struct memblock out_mem_11726;
    
    out_mem_11726.references = NULL;
    
    int32_t out_arrsizze_11727;
    
    lock_lock(&ctx->lock);
    v_mem_11670 = in0->mem;
    sizze_11462 = in0->shape[0];
    
    int ret = futrts_normalizzeV(ctx, &out_mem_11726, &out_arrsizze_11727,
                                 v_mem_11670, sizze_11462);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11726;
        (*out0)->shape[0] = out_arrsizze_11727;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorNorm(struct futhark_context *ctx, float *out0, const
                             struct futhark_f32_1d *in0)
{
    struct memblock v_mem_11670;
    
    v_mem_11670.references = NULL;
    
    int32_t sizze_11453;
    float scalar_out_11724;
    
    lock_lock(&ctx->lock);
    v_mem_11670 = in0->mem;
    sizze_11453 = in0->shape[0];
    
    int ret = futrts_vectorNorm(ctx, &scalar_out_11724, v_mem_11670,
                                sizze_11453);
    
    if (ret == 0) {
        *out0 = scalar_out_11724;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_dot(struct futhark_context *ctx, float *out0, const
                      struct futhark_f32_1d *in0, const
                      struct futhark_f32_1d *in1)
{
    struct memblock a_mem_11670;
    
    a_mem_11670.references = NULL;
    
    struct memblock b_mem_11671;
    
    b_mem_11671.references = NULL;
    
    int32_t i_11435;
    int32_t i_11436;
    float scalar_out_11722;
    
    lock_lock(&ctx->lock);
    a_mem_11670 = in0->mem;
    i_11435 = in0->shape[0];
    b_mem_11671 = in1->mem;
    i_11436 = in1->shape[0];
    
    int ret = futrts_dot(ctx, &scalar_out_11722, a_mem_11670, b_mem_11671,
                         i_11435, i_11436);
    
    if (ret == 0) {
        *out0 = scalar_out_11722;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_matrixSgn(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0)
{
    struct memblock x_mem_11670;
    
    x_mem_11670.references = NULL;
    
    int32_t sizze_11424;
    int32_t sizze_11425;
    struct memblock out_mem_11717;
    
    out_mem_11717.references = NULL;
    
    int32_t out_arrsizze_11718;
    int32_t out_arrsizze_11719;
    
    lock_lock(&ctx->lock);
    x_mem_11670 = in0->mem;
    sizze_11424 = in0->shape[0];
    sizze_11425 = in0->shape[1];
    
    int ret = futrts_matrixSgn(ctx, &out_mem_11717, &out_arrsizze_11718,
                               &out_arrsizze_11719, x_mem_11670, sizze_11424,
                               sizze_11425);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11717;
        (*out0)->shape[0] = out_arrsizze_11718;
        (*out0)->shape[1] = out_arrsizze_11719;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_scaleM(struct futhark_context *ctx,
                         struct futhark_f32_2d **out0, const float in0, const
                         struct futhark_f32_2d *in1)
{
    struct memblock x_mem_11670;
    
    x_mem_11670.references = NULL;
    
    int32_t sizze_11415;
    int32_t sizze_11416;
    float f_11417;
    struct memblock out_mem_11712;
    
    out_mem_11712.references = NULL;
    
    int32_t out_arrsizze_11713;
    int32_t out_arrsizze_11714;
    
    lock_lock(&ctx->lock);
    f_11417 = in0;
    x_mem_11670 = in1->mem;
    sizze_11415 = in1->shape[0];
    sizze_11416 = in1->shape[1];
    
    int ret = futrts_scaleM(ctx, &out_mem_11712, &out_arrsizze_11713,
                            &out_arrsizze_11714, x_mem_11670, sizze_11415,
                            sizze_11416, f_11417);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11712;
        (*out0)->shape[0] = out_arrsizze_11713;
        (*out0)->shape[1] = out_arrsizze_11714;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_matrixAbs(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0)
{
    struct memblock x_mem_11670;
    
    x_mem_11670.references = NULL;
    
    int32_t sizze_11407;
    int32_t sizze_11408;
    struct memblock out_mem_11707;
    
    out_mem_11707.references = NULL;
    
    int32_t out_arrsizze_11708;
    int32_t out_arrsizze_11709;
    
    lock_lock(&ctx->lock);
    x_mem_11670 = in0->mem;
    sizze_11407 = in0->shape[0];
    sizze_11408 = in0->shape[1];
    
    int ret = futrts_matrixAbs(ctx, &out_mem_11707, &out_arrsizze_11708,
                               &out_arrsizze_11709, x_mem_11670, sizze_11407,
                               sizze_11408);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_2d *) malloc(sizeof(struct futhark_f32_2d))) !=
            NULL);
        (*out0)->mem = out_mem_11707;
        (*out0)->shape[0] = out_arrsizze_11708;
        (*out0)->shape[1] = out_arrsizze_11709;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorMul(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0, const
                            struct futhark_f32_1d *in1)
{
    struct memblock a_mem_11670;
    
    a_mem_11670.references = NULL;
    
    struct memblock b_mem_11671;
    
    b_mem_11671.references = NULL;
    
    int32_t i_11392;
    int32_t i_11393;
    struct memblock out_mem_11704;
    
    out_mem_11704.references = NULL;
    
    int32_t out_arrsizze_11705;
    
    lock_lock(&ctx->lock);
    a_mem_11670 = in0->mem;
    i_11392 = in0->shape[0];
    b_mem_11671 = in1->mem;
    i_11393 = in1->shape[0];
    
    int ret = futrts_vectorMul(ctx, &out_mem_11704, &out_arrsizze_11705,
                               a_mem_11670, b_mem_11671, i_11392, i_11393);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11704;
        (*out0)->shape[0] = out_arrsizze_11705;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorSub(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0, const
                            struct futhark_f32_1d *in1)
{
    struct memblock a_mem_11670;
    
    a_mem_11670.references = NULL;
    
    struct memblock b_mem_11671;
    
    b_mem_11671.references = NULL;
    
    int32_t i_11377;
    int32_t i_11378;
    struct memblock out_mem_11701;
    
    out_mem_11701.references = NULL;
    
    int32_t out_arrsizze_11702;
    
    lock_lock(&ctx->lock);
    a_mem_11670 = in0->mem;
    i_11377 = in0->shape[0];
    b_mem_11671 = in1->mem;
    i_11378 = in1->shape[0];
    
    int ret = futrts_vectorSub(ctx, &out_mem_11701, &out_arrsizze_11702,
                               a_mem_11670, b_mem_11671, i_11377, i_11378);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11701;
        (*out0)->shape[0] = out_arrsizze_11702;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorAdd(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0, const
                            struct futhark_f32_1d *in1)
{
    struct memblock a_mem_11670;
    
    a_mem_11670.references = NULL;
    
    struct memblock b_mem_11671;
    
    b_mem_11671.references = NULL;
    
    int32_t i_11362;
    int32_t i_11363;
    struct memblock out_mem_11698;
    
    out_mem_11698.references = NULL;
    
    int32_t out_arrsizze_11699;
    
    lock_lock(&ctx->lock);
    a_mem_11670 = in0->mem;
    i_11362 = in0->shape[0];
    b_mem_11671 = in1->mem;
    i_11363 = in1->shape[0];
    
    int ret = futrts_vectorAdd(ctx, &out_mem_11698, &out_arrsizze_11699,
                               a_mem_11670, b_mem_11671, i_11362, i_11363);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11698;
        (*out0)->shape[0] = out_arrsizze_11699;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorSgn(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0)
{
    struct memblock x_mem_11670;
    
    x_mem_11670.references = NULL;
    
    int32_t sizze_11354;
    struct memblock out_mem_11695;
    
    out_mem_11695.references = NULL;
    
    int32_t out_arrsizze_11696;
    
    lock_lock(&ctx->lock);
    x_mem_11670 = in0->mem;
    sizze_11354 = in0->shape[0];
    
    int ret = futrts_vectorSgn(ctx, &out_mem_11695, &out_arrsizze_11696,
                               x_mem_11670, sizze_11354);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11695;
        (*out0)->shape[0] = out_arrsizze_11696;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_scaleV(struct futhark_context *ctx,
                         struct futhark_f32_1d **out0, const float in0, const
                         struct futhark_f32_1d *in1)
{
    struct memblock x_mem_11670;
    
    x_mem_11670.references = NULL;
    
    int32_t sizze_11348;
    float f_11349;
    struct memblock out_mem_11692;
    
    out_mem_11692.references = NULL;
    
    int32_t out_arrsizze_11693;
    
    lock_lock(&ctx->lock);
    f_11349 = in0;
    x_mem_11670 = in1->mem;
    sizze_11348 = in1->shape[0];
    
    int ret = futrts_scaleV(ctx, &out_mem_11692, &out_arrsizze_11693,
                            x_mem_11670, sizze_11348, f_11349);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11692;
        (*out0)->shape[0] = out_arrsizze_11693;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
int futhark_entry_vectorAbs(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0)
{
    struct memblock x_mem_11670;
    
    x_mem_11670.references = NULL;
    
    int32_t sizze_11343;
    struct memblock out_mem_11689;
    
    out_mem_11689.references = NULL;
    
    int32_t out_arrsizze_11690;
    
    lock_lock(&ctx->lock);
    x_mem_11670 = in0->mem;
    sizze_11343 = in0->shape[0];
    
    int ret = futrts_vectorAbs(ctx, &out_mem_11689, &out_arrsizze_11690,
                               x_mem_11670, sizze_11343);
    
    if (ret == 0) {
        assert((*out0 =
                (struct futhark_f32_1d *) malloc(sizeof(struct futhark_f32_1d))) !=
            NULL);
        (*out0)->mem = out_mem_11689;
        (*out0)->shape[0] = out_arrsizze_11690;
    }
    lock_unlock(&ctx->lock);
    return ret;
}
