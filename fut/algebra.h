#pragma once

/*
 * Headers
*/

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>


/*
 * Initialisation
*/

struct futhark_context_config ;
struct futhark_context_config *futhark_context_config_new();
void futhark_context_config_free(struct futhark_context_config *cfg);
void futhark_context_config_set_debugging(struct futhark_context_config *cfg,
                                          int flag);
void futhark_context_config_set_logging(struct futhark_context_config *cfg,
                                        int flag);
struct futhark_context ;
struct futhark_context *futhark_context_new(struct futhark_context_config *cfg);
void futhark_context_free(struct futhark_context *ctx);
int futhark_context_sync(struct futhark_context *ctx);
char *futhark_context_get_error(struct futhark_context *ctx);
void futhark_context_pause_profiling(struct futhark_context *ctx);
void futhark_context_unpause_profiling(struct futhark_context *ctx);

/*
 * Arrays
*/

struct futhark_f32_1d ;
struct futhark_f32_1d *futhark_new_f32_1d(struct futhark_context *ctx,
                                          float *data, int64_t dim0);
struct futhark_f32_1d *futhark_new_raw_f32_1d(struct futhark_context *ctx,
                                              char *data, int offset,
                                              int64_t dim0);
int futhark_free_f32_1d(struct futhark_context *ctx,
                        struct futhark_f32_1d *arr);
int futhark_values_f32_1d(struct futhark_context *ctx,
                          struct futhark_f32_1d *arr, float *data);
char *futhark_values_raw_f32_1d(struct futhark_context *ctx,
                                struct futhark_f32_1d *arr);
int64_t *futhark_shape_f32_1d(struct futhark_context *ctx,
                              struct futhark_f32_1d *arr);
struct futhark_f32_2d ;
struct futhark_f32_2d *futhark_new_f32_2d(struct futhark_context *ctx,
                                          float *data, int64_t dim0,
                                          int64_t dim1);
struct futhark_f32_2d *futhark_new_raw_f32_2d(struct futhark_context *ctx,
                                              char *data, int offset,
                                              int64_t dim0, int64_t dim1);
int futhark_free_f32_2d(struct futhark_context *ctx,
                        struct futhark_f32_2d *arr);
int futhark_values_f32_2d(struct futhark_context *ctx,
                          struct futhark_f32_2d *arr, float *data);
char *futhark_values_raw_f32_2d(struct futhark_context *ctx,
                                struct futhark_f32_2d *arr);
int64_t *futhark_shape_f32_2d(struct futhark_context *ctx,
                              struct futhark_f32_2d *arr);

/*
 * Opaque values
*/


/*
 * Entry points
*/

int futhark_entry_matrixMatrixMul(struct futhark_context *ctx,
                                  struct futhark_f32_2d **out0, const
                                  struct futhark_f32_2d *in0, const
                                  struct futhark_f32_2d *in1);
int futhark_entry_vectorMatrixMul(struct futhark_context *ctx,
                                  struct futhark_f32_1d **out0, const
                                  struct futhark_f32_1d *in0, const
                                  struct futhark_f32_2d *in1);
int futhark_entry_matrixMul(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0, const
                            struct futhark_f32_2d *in1);
int futhark_entry_matrixSub(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0, const
                            struct futhark_f32_2d *in1);
int futhark_entry_matrixAdd(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0, const
                            struct futhark_f32_2d *in1);
int futhark_entry_matrixVectorMul(struct futhark_context *ctx,
                                  struct futhark_f32_1d **out0, const
                                  struct futhark_f32_2d *in0, const
                                  struct futhark_f32_1d *in1);
int futhark_entry_normalizeV(struct futhark_context *ctx,
                             struct futhark_f32_1d **out0, const
                             struct futhark_f32_1d *in0);
int futhark_entry_vectorNorm(struct futhark_context *ctx, float *out0, const
                             struct futhark_f32_1d *in0);
int futhark_entry_dot(struct futhark_context *ctx, float *out0, const
                      struct futhark_f32_1d *in0, const
                      struct futhark_f32_1d *in1);
int futhark_entry_matrixSgn(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0);
int futhark_entry_scaleM(struct futhark_context *ctx,
                         struct futhark_f32_2d **out0, const float in0, const
                         struct futhark_f32_2d *in1);
int futhark_entry_matrixAbs(struct futhark_context *ctx,
                            struct futhark_f32_2d **out0, const
                            struct futhark_f32_2d *in0);
int futhark_entry_vectorMul(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0, const
                            struct futhark_f32_1d *in1);
int futhark_entry_vectorSub(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0, const
                            struct futhark_f32_1d *in1);
int futhark_entry_vectorAdd(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0, const
                            struct futhark_f32_1d *in1);
int futhark_entry_vectorSgn(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0);
int futhark_entry_scaleV(struct futhark_context *ctx,
                         struct futhark_f32_1d **out0, const float in0, const
                         struct futhark_f32_1d *in1);
int futhark_entry_vectorAbs(struct futhark_context *ctx,
                            struct futhark_f32_1d **out0, const
                            struct futhark_f32_1d *in0);

/*
 * Miscellaneous
*/

void futhark_debugging_report(struct futhark_context *ctx);
