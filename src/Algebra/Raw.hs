{-# LANGUAGE ForeignFunctionInterface #-}
module Algebra.Raw where
import Data.Int (Int8, Int16, Int32, Int64)
import Data.Word (Word8, Word16, Word32, Word64)
import Foreign.C.Types (CBool(..), CSize(..), CChar(..))
import Foreign.Ptr (Ptr)

-- Headers



-- Initialisation
data Futhark_context_config
foreign import ccall unsafe "futhark_context_config_new"
  context_config_new
    :: IO (Ptr Futhark_context_config)

foreign import ccall unsafe "futhark_context_config_free"
  context_config_free
    :: Ptr Futhark_context_config
    -> IO ()

foreign import ccall unsafe "futhark_context_config_set_debugging"
  context_config_set_debugging
    :: Ptr Futhark_context_config
    -> Int
    -> IO ()

foreign import ccall unsafe "futhark_context_config_set_logging"
  context_config_set_logging
    :: Ptr Futhark_context_config
    -> Int
    -> IO ()

data Futhark_context
foreign import ccall unsafe "futhark_context_new"
  context_new
    :: Ptr Futhark_context_config
    -> IO (Ptr Futhark_context)

foreign import ccall unsafe "futhark_context_free"
  context_free
    :: Ptr Futhark_context
    -> IO ()

foreign import ccall unsafe "futhark_context_sync"
  context_sync
    :: Ptr Futhark_context
    -> IO Int

foreign import ccall unsafe "futhark_context_get_error"
  context_get_error
    :: Ptr Futhark_context
    -> IO (Ptr CChar)

foreign import ccall unsafe "futhark_context_pause_profiling"
  context_pause_profiling
    :: Ptr Futhark_context
    -> IO ()

foreign import ccall unsafe "futhark_context_unpause_profiling"
  context_unpause_profiling
    :: Ptr Futhark_context
    -> IO ()

-- Arrays
data Futhark_f32_1d
foreign import ccall unsafe "futhark_new_f32_1d"
  new_f32_1d
    :: Ptr Futhark_context
    -> Ptr Float
    -> Int64
    -> IO (Ptr Futhark_f32_1d)

foreign import ccall unsafe "futhark_new_raw_f32_1d"
  new_raw_f32_1d
    :: Ptr Futhark_context
    -> Ptr CChar
    -> Int
    -> Int64
    -> IO (Ptr Futhark_f32_1d)

foreign import ccall unsafe "futhark_free_f32_1d"
  free_f32_1d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_values_f32_1d"
  values_f32_1d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_1d
    -> Ptr Float
    -> IO Int

foreign import ccall unsafe "futhark_values_raw_f32_1d"
  values_raw_f32_1d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_1d
    -> IO (Ptr CChar)

foreign import ccall unsafe "futhark_shape_f32_1d"
  shape_f32_1d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_1d
    -> IO (Ptr Int64)

data Futhark_f32_2d
foreign import ccall unsafe "futhark_new_f32_2d"
  new_f32_2d
    :: Ptr Futhark_context
    -> Ptr Float
    -> Int64
    -> Int64
    -> IO (Ptr Futhark_f32_2d)

foreign import ccall unsafe "futhark_new_raw_f32_2d"
  new_raw_f32_2d
    :: Ptr Futhark_context
    -> Ptr CChar
    -> Int
    -> Int64
    -> Int64
    -> IO (Ptr Futhark_f32_2d)

foreign import ccall unsafe "futhark_free_f32_2d"
  free_f32_2d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_values_f32_2d"
  values_f32_2d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_2d
    -> Ptr Float
    -> IO Int

foreign import ccall unsafe "futhark_values_raw_f32_2d"
  values_raw_f32_2d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_2d
    -> IO (Ptr CChar)

foreign import ccall unsafe "futhark_shape_f32_2d"
  shape_f32_2d
    :: Ptr Futhark_context
    -> Ptr Futhark_f32_2d
    -> IO (Ptr Int64)

-- Opaque values
-- Entry points
foreign import ccall unsafe "futhark_entry_matrixMatrixMul"
  entry_matrixMatrixMul
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Ptr Futhark_f32_2d
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorMatrixMul"
  entry_vectorMatrixMul
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_matrixMul"
  entry_matrixMul
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Ptr Futhark_f32_2d
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_matrixSub"
  entry_matrixSub
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Ptr Futhark_f32_2d
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_matrixAdd"
  entry_matrixAdd
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Ptr Futhark_f32_2d
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_matrixVectorMul"
  entry_matrixVectorMul
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_2d
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_normalizeV"
  entry_normalizeV
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorNorm"
  entry_vectorNorm
    :: Ptr Futhark_context
    -> Ptr Float
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_dot"
  entry_dot
    :: Ptr Futhark_context
    -> Ptr Float
    -> Ptr Futhark_f32_1d
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_matrixSgn"
  entry_matrixSgn
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_scaleM"
  entry_scaleM
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Float
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_matrixAbs"
  entry_matrixAbs
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_2d)
    -> Ptr Futhark_f32_2d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorMul"
  entry_vectorMul
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorSub"
  entry_vectorSub
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorAdd"
  entry_vectorAdd
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorSgn"
  entry_vectorSgn
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_scaleV"
  entry_scaleV
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Float
    -> Ptr Futhark_f32_1d
    -> IO Int

foreign import ccall unsafe "futhark_entry_vectorAbs"
  entry_vectorAbs
    :: Ptr Futhark_context
    -> Ptr (Ptr Futhark_f32_1d)
    -> Ptr Futhark_f32_1d
    -> IO Int

-- Miscellaneous
foreign import ccall unsafe "futhark_debugging_report"
  debugging_report
    :: Ptr Futhark_context
    -> IO ()
